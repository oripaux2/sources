package ext.eurocopter.x4.loaders.ecloader;

import java.io.Serializable;

/**
 * @author a805391
 *
 */
public class AlternateDef implements Serializable {
	private static final long serialVersionUID = -4437154980367888829L;
	private String originpn;
	private String targetpn;
	public AlternateDef(String originpn, String targetpn) {
		super();
		this.originpn = originpn;
		this.targetpn = targetpn;
	}
	public String getOriginpn() {
		return originpn;
	}
	public String getTargetpn() {
		return targetpn;
	}
	
}
