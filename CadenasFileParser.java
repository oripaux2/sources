package ext.eurocopter.x4.loaders.ecloader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.logging.Logger;

import com.ptc.core.meta.common.FloatingPoint;

import ext.eurocopter.utils.StringUtils;

public class CadenasFileParser {
	
	Logger LOGGER = Logger.getAnonymousLogger();
	
	//private static int CADENAS_CHAR_END=4413;
	private static int F00_ARTICLE_STID=0;
	private static int F01_VALFAM_STID=19;
	private static int F02_NPF_STID=50;//mpn
	private static int F03_FAB_STID=91;
	private static int F04_FABCLEREC_STID=102;
	private static int F05_FABNOM_STID=113;
	//private static int F06_FABLOC_STID=149;
	private static int F07_UQ_STID=185;
	private static int F08_NOARTICLECMS=189;
	private static int F09_GROUPEMARCHAND=208;
//	private static int F10_DESIGNATIONFR=218;
//	private static int F11_DESIGNATIONEN=259;
//	private static int F12_DESIGNATIONGE=300;
//	private static int F13_DESIGNATIONES=341;
//	private static int F14_SA=382;
//	private static int F15_LAB=385;
	private static int F16_STATUS=389;
	private static int F17_GROUPEMARCHANDEXT=392;
	private static int F18_DESIGNATIONNORMALISEE=411;
	private static int F19_PBRUT=430;
	private static int F20_PNET=444;
	private static int F21_WEIGHTUNIT=458;
	private static int F22_MATERIAL=462;
//	private static int F23_TAILLEDIM=511;
	private static int F24_TEXTDONBAS_FR=544;
	private static int F25_TEXTDONBAS_EN=605;
	private static int F26_TEXTDONBAS_DE=666;
	private static int F27_TEXTDONBAS_ES=727;
	private static int F28_REPLACEMENTTYPE=788;
	private static int F29_REPLACEMENT_MATNR=790;
	private static int F30_REPLACEMENT_MPN=809;
//	private static int F31_REPLACEMENT_SA=842;
//	private static int F32_REPLACEMENT_FAB=845;
//	private static int F33_REPLACEMENT_CRITERIA=856;
//	private static int F34_NORM_DENOMINATION=867;
//	private static int F35_NORM_DEFINITION_COMPL=895;
//	private static int F36_NORM_MATERIAL=956;
//	private static int F37_NORM_VALIDITY=975;
//	private static int F38_NORM_LIVRET=977;
//	private static int F39_NORM_CHOIX=980;
	private static int F40_RT_PR1=985;
	private static int F41_RT_VA1=1018;
	private static int F42_RT_PR2=1078;
	private static int F43_RT_VA2=1111;
	private static int F44_RT_PR3=1171;
	private static int F45_RT_VA3=1204;
	private static int F46_RT_PR4=1264;
	private static int F47_RT_VA4=1297;
	private static int F48_RT_PR5=1357;
	private static int F49_RT_VA5=1390;
	private static int F50_RT_PR6=1450;
	private static int F51_RT_VA6=1483;
	private static int F52_RT_PR7=1543;
	private static int F53_RT_VA7=1576;
	private static int F54_RT_PR8=1636;
	private static int F55_RT_VA8=1669;
	private static int F54b_RT_PR9=1729;
	private static int F55b_RT_VA9=1789;
	private static int F55c_NORM_DENOM_EN=1849;
//	private static int F56_NORM_COMP_DENOM_EN=1877;
	private static int F57_NORM_VALIDITY=1938;
	private static int F58_ART_VALIDITY=2194;
	private static int F59_DIVISIONS=2450;
//	private static int F60_LISTEDOCS=2571;
//	private static int F61_TYPEDOCS=2827;
	private static int F100_MOVPRICE_0210=4251;
	private static int F101_UNRSTOCK_0210=4263;
	private static int F102_PLANPRICE_1000=4277;
	private static int F103_MOVPRICE_1000=4319;
	private static int F104_UNRSTOCK_1000=4331;
	private static int F105_EC_USAGE=4345;
	
	private static int F147_HAZ_MAT_IND=5951;
	private static int F148_END_OF_FILE=5954;
	
	public static String STANDARD_PART_FAMILY = "STANDARD PART";
	public static String NON_AERO_STD_PART_FAMILLY = "MAINTENANCE PURCHASED PART";
	
	private BufferedReader majorparsingreader;
	private static final Collection<String> KILOGRAM_UNIT_LABEL_LIST = Arrays.asList(new String[] {
	        "KG", "KGN", "KGW", "KGC", "KGE"
	    });
	
	private File cadenasfile;
	public CadenasFileParser(File cadenasfile) {
		this.cadenasfile=cadenasfile;
	}
	public void InitiateMajorparsing() throws Exception {
		majorparsingreader = new BufferedReader(new FileReader(cadenasfile));
		majorparsingreader.readLine(); // discards the first header line
	}
	public void CloseMajorParsing() throws Exception {
		majorparsingreader.close();
	}
	public static String trimleftzero(String value) {
		if (value==null) return null;
		if (value.length()==0) return value;
		int firstnonzeroindex=0;
		for (int i=0;i<value.length();i++) {
			if (value.charAt(i)=='0') {
				firstnonzeroindex=i;
			} else {
				i=value.length();
			}
		}
		if (firstnonzeroindex+1==value.length()) return "";
		return StringUtils.optimizedSubstring(value,firstnonzeroindex+1);	
		}
	
	/**
	 * creates a standard fed from a valid 
	 * @param line one valid line of a CADENAS file
	 * @return a standarddef
	 */
	public StandardDef parseOneCadenasStandard(String line,String suffix) {
		// MATNR - trim and remove left zeros
		String matnr=(StringUtils.optimizedSubstring(line,F00_ARTICLE_STID,F01_VALFAM_STID-1).trim()+suffix).toUpperCase();
		matnr=trimleftzero(matnr);
		
		// STATUS OF REFERENCE - trim and transform
		String statusreference = StringUtils.optimizedSubstring(line,F01_VALFAM_STID,F02_NPF_STID-1).trim();
		if(statusreference !=null)
			statusreference=statusreference.toUpperCase();
		String partClassification = null;
		boolean replacedstandardpart=false; // only for links z, they are not present here
		String standardPartSoftype = StandardDef.STANDARD_SOFTTYPE_STD;
		if (STANDARD_PART_FAMILY.compareTo(statusreference)==0) {
			statusreference =StandardDef.STATUSREF_STANDARDPART;
			replacedstandardpart=false; 
			standardPartSoftype = StandardDef.STANDARD_SOFTTYPE_STD;
		}else if(NON_AERO_STD_PART_FAMILLY.compareTo(statusreference)==0) {
			// PDM_R&M-STD-19_4006 for non aero parts
			statusreference = StandardDef.STATUSREF_NON_AERO_STD_PART;
			//PDM_R&M-STD-19_4004 partClassification value in case of non aero parts
			partClassification = StandardDef.PART_CLASSIFICATION_GSE_AGE_TOOLS;
			//PDM_R&M-STD-19_4006
			standardPartSoftype = StandardDef.STANDARD_SOFTTYPE_NON_AERO;
			replacedstandardpart=false; 
		} 
		else {
			// by default, put it to restricted standard part
			//REPLACED STANDARD PART 
			statusreference = StandardDef.STATUSREF_REPLACEDSTANDARDPART;
			standardPartSoftype = StandardDef.STANDARD_SOFTTYPE_STD;
		}
		
		// MPN - trim and capital letter
		String mpn = (StringUtils.optimizedSubstring(line,F02_NPF_STID,F03_FAB_STID-1).trim()+suffix).toUpperCase();
		
		// NCAGE - only trim and suffix
		String ncage=StringUtils.optimizedSubstring(line,F04_FABCLEREC_STID,F05_FABNOM_STID-1).trim()+suffix;
		
		// Lifecycle state
		String sapstatus=StringUtils.optimizedSubstring(line,F16_STATUS,F17_GROUPEMARCHANDEXT-1).trim();
		
		String lifecyclestate = StandardDef.LIFECYCLE_NOTRELEASEDFORNEWSTUDY;
		if (sapstatus.compareTo("1")==0) lifecyclestate = StandardDef.LIFECYCLE_PRELIMINARY;
		if (sapstatus.compareTo("3")==0||sapstatus.compareTo("60")==0) lifecyclestate = StandardDef.LIFECYCLE_SERIALRELEASED;
		if (sapstatus.compareTo("9")==0) lifecyclestate = StandardDef.LIFECYCLE_CANCELLED;
		
		// CMS - no trim to preserve left spaces
		String cms=StringUtils.optimizedSubstring(line,F08_NOARTICLECMS,F09_GROUPEMARCHAND-1);
		
		// qualified inside SDR perimeter
		String qualifiedinsidesdrperimeter=StandardDef.SDRPERIMETER_ECD;
		if (cms.trim().length()>0)
			qualifiedinsidesdrperimeter=StandardDef.SDRPERIMETER_ECG;
		
		// French designation trim and limit to 40
		String frenchdesc= StringUtils.optimizedSubstring(line,F24_TEXTDONBAS_FR,F25_TEXTDONBAS_EN-1).trim();
		if (frenchdesc.length()>40) frenchdesc=StringUtils.optimizedSubstring(frenchdesc,0,40);
		
		// English designation trim and limit to 40
		String englishdesc=StringUtils.optimizedSubstring(line,F25_TEXTDONBAS_EN,F26_TEXTDONBAS_DE-1).trim();
		if (englishdesc.length()>40) englishdesc=StringUtils.optimizedSubstring(englishdesc,0,40);
		
		// German designation - trim and limit to 40
		String germandesc=StringUtils.optimizedSubstring(line,F26_TEXTDONBAS_DE,F27_TEXTDONBAS_ES-1).trim();
		if (germandesc.length()>40) germandesc=StringUtils.optimizedSubstring(germandesc,0,40);
		
		// Spanish designation - trim and limit to 40
		String spanishdesc=StringUtils.optimizedSubstring(line,F27_TEXTDONBAS_ES,F28_REPLACEMENTTYPE-1).trim();
		if (spanishdesc.length()>40) spanishdesc=StringUtils.optimizedSubstring(spanishdesc,0,40);
		
		// Standard reference number
		String standardreferencenumber=StringUtils.optimizedSubstring(line,F18_DESIGNATIONNORMALISEE,F19_PBRUT-1).trim();
		
		// Weight - only trim - Assumes valid number 
		String standardweight=StringUtils.optimizedSubstring(line,F20_PNET,F21_WEIGHTUNIT-1).trim();
		if (standardweight.compareTo("0.000")==0)
			{
			LOGGER.warning("WARNING CAUSE : \""+ mpn +"\" ;Weight = \""+ standardweight +"\"");
			standardweight="0.001";
			}
	
		// Weight Unit - implements mapping
		String bum=StringUtils.optimizedSubstring(line,F07_UQ_STID,F08_NOARTICLECMS-1).trim();
		String standardweightunit=null;
		if (bum.compareTo("PC")==0) standardweightunit=StandardDef.WEIGHTUNIT_GPC;
		if (bum.compareTo("PCE")==0) standardweightunit=StandardDef.WEIGHTUNIT_GPC;
		if (bum.compareTo("ST")==0) standardweightunit=StandardDef.WEIGHTUNIT_GPC;
		if (bum.compareTo("M")==0) standardweightunit=StandardDef.WEIGHTUNIT_GM;
		if (bum.compareTo("M2")==0) standardweightunit=StandardDef.WEIGHTUNIT_GM2;
		if (bum.compareTo("L")==0) standardweightunit=StandardDef.WEIGHTUNIT_GL;
		if (bum.compareTo("KG")==0) standardweightunit=StandardDef.WEIGHTUNIT_G;
		
		// Weight maturity - implements mapping
		String wun=StringUtils.optimizedSubstring(line,F21_WEIGHTUNIT,F22_MATERIAL-1).trim();
		String weightmaturity=null;
		// calculated
		if (wun.compareTo("GNO")==0) weightmaturity=StandardDef.WEIGHTMATURITY_CALCULATED;
		if (wun.compareTo("GCA")==0) weightmaturity=StandardDef.WEIGHTMATURITY_CALCULATED;
		if (wun.compareTo("KGC")==0) weightmaturity=StandardDef.WEIGHTMATURITY_CALCULATED;
		if (wun.compareTo("KGN")==0) weightmaturity=StandardDef.WEIGHTMATURITY_CALCULATED;
		// estimated
		if (wun.compareTo("GES")==0) weightmaturity=StandardDef.WEIGHTMATURITY_ESTIMATED;
		if (wun.compareTo("G")==0) weightmaturity=StandardDef.WEIGHTMATURITY_ESTIMATED;
		if (wun.compareTo("KGE")==0) weightmaturity=StandardDef.WEIGHTMATURITY_ESTIMATED;
		if (wun.compareTo("KG")==0) weightmaturity=StandardDef.WEIGHTMATURITY_ESTIMATED;
		// weighted
		if (wun.compareTo("GWE")==0) weightmaturity=StandardDef.WEIGHTMATURITY_WEIGHTED;
		if (wun.compareTo("KGW")==0) weightmaturity=StandardDef.WEIGHTMATURITY_WEIGHTED;
		if(KILOGRAM_UNIT_LABEL_LIST.contains(wun))
        {
            double standardweightDouble = Double.valueOf(standardweight).doubleValue();
            standardweight = String.valueOf(standardweightDouble * 1000D);
        }
		// RT Attributes
		ArrayList<String> additionalattributes=new ArrayList<String>();
		
		String parrt1=StringUtils.optimizedSubstring(line,F40_RT_PR1,F41_RT_VA1-1).trim();
		if (parrt1.length()>0) {
			String valrt1=StringUtils.optimizedSubstring(line,F41_RT_VA1,F42_RT_PR2-1).trim();
			String attrstring = parrt1+" = "+valrt1;
			additionalattributes.add(attrstring);
		}

		String parrt2=StringUtils.optimizedSubstring(line,F42_RT_PR2,F43_RT_VA2-1).trim();
		if (parrt2.length()>0) {
			String valrt2=StringUtils.optimizedSubstring(line,F43_RT_VA2,F44_RT_PR3-1).trim();
			String attrstring = parrt2+" = "+valrt2;
			additionalattributes.add(attrstring);
		}
		
		String parrt3=StringUtils.optimizedSubstring(line,F44_RT_PR3,F45_RT_VA3-1).trim();
		if (parrt3.length()>0) {
			String valrt3=StringUtils.optimizedSubstring(line,F45_RT_VA3,F46_RT_PR4-1).trim();
			String attrstring = parrt3+" = "+valrt3;
			additionalattributes.add(attrstring);
		}
		
		String parrt4=StringUtils.optimizedSubstring(line,F46_RT_PR4,F47_RT_VA4-1).trim();
		if (parrt4.length()>0) {
			String valrt4=StringUtils.optimizedSubstring(line,F47_RT_VA4,F48_RT_PR5-1).trim();
			String attrstring = parrt4+" = "+valrt4;
			additionalattributes.add(attrstring);
		}
		
		String parrt5=StringUtils.optimizedSubstring(line,F48_RT_PR5,F49_RT_VA5-1).trim();
		if (parrt5.length()>0) {
			String valrt5=StringUtils.optimizedSubstring(line,F49_RT_VA5,F50_RT_PR6-1).trim();
			String attrstring = parrt5+" = "+valrt5;
			additionalattributes.add(attrstring);
		}
		
		String parrt6=StringUtils.optimizedSubstring(line,F50_RT_PR6,F51_RT_VA6-1).trim();
		if (parrt6.length()>0) {
			String valrt6=StringUtils.optimizedSubstring(line,F51_RT_VA6,F52_RT_PR7-1).trim();
			String attrstring = parrt6+" = "+valrt6;
			additionalattributes.add(attrstring);
		}
		
		String parrt7=StringUtils.optimizedSubstring(line,F52_RT_PR7,F53_RT_VA7-1).trim();
		if (parrt7.length()>0) {
			String valrt7=StringUtils.optimizedSubstring(line,F53_RT_VA7,F54_RT_PR8-1).trim();
			String attrstring = parrt7+" = "+valrt7;
			additionalattributes.add(attrstring);
		}
		
		String parrt8=StringUtils.optimizedSubstring(line,F54_RT_PR8,F55_RT_VA8-1).trim();
		if (parrt8.length()>0) {
			String valrt8=StringUtils.optimizedSubstring(line,F55_RT_VA8,F54b_RT_PR9-1).trim();
			String attrstring = parrt8+" = "+valrt8;
			additionalattributes.add(attrstring);
		}
		
		String parrt9=StringUtils.optimizedSubstring(line,F54b_RT_PR9,F55b_RT_VA9-1).trim();
		if (parrt9.length()>0) {
			String valrt9=StringUtils.optimizedSubstring(line,F55b_RT_VA9,F55c_NORM_DENOM_EN-1).trim();
			String attrstring = parrt9+" = "+valrt9;
			additionalattributes.add(attrstring);
		}
		
		// Material validity
		String materialvalidity=StringUtils.optimizedSubstring(line,F57_NORM_VALIDITY,F58_ART_VALIDITY-1).trim();
		String partvalidity=StringUtils.optimizedSubstring(line,F58_ART_VALIDITY,F59_DIVISIONS-1).trim();
		
		String[] splitmaterialval=materialvalidity.split("\\+");
		String[] splitpartvalidity=partvalidity.split("\\+");
		
		ArrayList<String> validityarray=new ArrayList<String>();
		// only used to check unicity;
		HashMap<String,String> alreadypresent=new HashMap<String,String>();
		if (splitmaterialval!=null) for (int i=0;i<splitmaterialval.length;i++) {
			if (alreadypresent.get(splitmaterialval[i])==null) {
				if (splitmaterialval[i].length()>0) {
					alreadypresent.put(splitmaterialval[i],splitmaterialval[i]);
					validityarray.add(splitmaterialval[i]);
				}
			}
		}
		if (splitpartvalidity!=null) for (int i=0;i<splitpartvalidity.length;i++) {
			if (alreadypresent.get(splitpartvalidity[i])==null) {
				if (splitpartvalidity[i].length()>0) {
					alreadypresent.put(splitpartvalidity[i],splitpartvalidity[i]);
					validityarray.add(splitpartvalidity[i]);
				}
			}
		}
		
		// activated EC
		String activatedec="No";
		String movingpriceec=StringUtils.optimizedSubstring(line,F100_MOVPRICE_0210,F101_UNRSTOCK_0210-1).trim();
		String unrstockec=StringUtils.optimizedSubstring(line,F101_UNRSTOCK_0210,F102_PLANPRICE_1000-1).trim();
		Float checkec = FloatingPoint.valueOf(movingpriceec).floatValue()+
		FloatingPoint.valueOf(unrstockec).floatValue();
		
		if (checkec>=0.001) activatedec="Yes";
		LOGGER.finest("movingpriceec = "+movingpriceec+", urstockec = "+unrstockec+", total = "+checkec+", decision = "+activatedec);

		// activatedecd
		String activatedecd="No";
		String movingpriceecd=StringUtils.optimizedSubstring(line,F103_MOVPRICE_1000,F104_UNRSTOCK_1000-1).trim();
		String unrstockecd=StringUtils.optimizedSubstring(line,F104_UNRSTOCK_1000, F105_EC_USAGE-1).trim();
		
		Float checkecd = FloatingPoint.valueOf(movingpriceecd).floatValue()+ FloatingPoint.valueOf(unrstockecd).floatValue();
		if (checkecd>=0.001) activatedecd="Yes";
		
		//hazard material indicator
		String hazMatInd_sapValue = StringUtils.optimizedSubstring(line,F147_HAZ_MAT_IND,F148_END_OF_FILE-1).trim();
		String hazMatInd=null;
		// calculate windchill value
		if (hazMatInd_sapValue.compareTo("00")==0) hazMatInd=StandardDef.HAZ_MAT_IND_UNKNOW;
		if (hazMatInd_sapValue.compareTo("01")==0) hazMatInd=StandardDef.HAZ_MAT_IND_NOT_COMP;
		if (hazMatInd_sapValue.compareTo("02")==0) hazMatInd=StandardDef.HAZ_MAT_IND_NOT_COMP;
		if (hazMatInd_sapValue.compareTo("03")==0) hazMatInd=StandardDef.HAZ_MAT_IND_PART_COMP;
		if (hazMatInd_sapValue.compareTo("04")==0) hazMatInd=StandardDef.HAZ_MAT_IND_COMP;
		if (hazMatInd_sapValue.compareTo("05")==0) hazMatInd=StandardDef.HAZ_MAT_IND_UNKNOW;
		
		LOGGER.finest("movingpriceecd = "+movingpriceecd+", urstockecd = "+unrstockecd+", total = "+checkecd+", decision = "+activatedecd);
		//System.out.println(matnr+"----"+ncage+"----"+mpn+"----"+englishdesc+"----"+germandesc+"----"+frenchdesc+"----"+spanishdesc+"----"+standardreferencenumber+"----"+statusreference+"----"+qualifiedinsidesdrperimeter+"----"+lifecyclestate+"----"+standardweight+"----"+weightmaturity+"----"+standardweightunit+"----"+cms+"----"+activatedecd+"----"+activatedec+"----"+ standardPartSoftype+"----"+partClassification+"----"+ replacedstandardpart+"----"+additionalattributes.toArray(new String[0])+"----"+validityarray.toArray(new String[0]));	
		return new StandardDef(matnr,
					ncage, 
					mpn,
					englishdesc,
					germandesc,
					frenchdesc,
					spanishdesc,
					standardreferencenumber,
					statusreference,
					qualifiedinsidesdrperimeter,
					lifecyclestate,
					standardweight,
					weightmaturity,
					standardweightunit,
					cms,activatedecd,activatedec, standardPartSoftype, 
					partClassification, replacedstandardpart,
					additionalattributes.toArray(new String[0]),
					validityarray.toArray(new String[0]),hazMatInd
					);
	}
	
	public ArrayList<StandardDef> parseNewStandardBatch(int limit,String suffix) throws Exception {
		
		ArrayList<StandardDef> standardbatch = new ArrayList<StandardDef>();
		String line=majorparsingreader.readLine();
		int linenr=0;
		while (line!=null) {
			//if (line.length()!=CADENAS_CHAR_END) throw new Exception ("input file corrupted, incorrect size = "+line.length()+", line number = "+linenr);
			linenr++;
			if(!line.trim().equals(""))
				standardbatch.add(parseOneCadenasStandard(line,suffix));
			if (linenr<limit) {
				line=majorparsingreader.readLine();
			} else {
				line=null;
			}
		}
		return standardbatch;
	}

	public ArrayList<AlternateDef> parseFileForAlternates(String testsuffix) throws Exception {
		BufferedReader reader = new BufferedReader(new FileReader(cadenasfile));
		ArrayList<AlternateDef> alternatelist=new ArrayList<AlternateDef>();
		if (testsuffix==null) testsuffix="";
		String line = reader.readLine();
		while ((line=reader.readLine())!=null) {
			if(!line.trim().equals("")){
				//if (line.length()!=CADENAS_CHAR_END) throw new Exception ("input file corrupted, incorrect size = "+line.length()+", line number = "+linenr);
				String originmatnr=(StringUtils.optimizedSubstring(line,F00_ARTICLE_STID,F01_VALFAM_STID-1).trim()+testsuffix).toUpperCase();
				originmatnr=trimleftzero(originmatnr);
				String targetmatnr=StringUtils.optimizedSubstring(line,F29_REPLACEMENT_MATNR,F30_REPLACEMENT_MPN-1).trim();
				targetmatnr=trimleftzero(targetmatnr);
				if (targetmatnr.length()>0) targetmatnr=(targetmatnr+testsuffix).toUpperCase();
				AlternateDef thisalternate = new AlternateDef(originmatnr,targetmatnr);
				alternatelist.add(thisalternate);
			}
		}
		reader.close();
		return alternatelist;
	}
	
	/**
	 * Parses the whole file for suppliers. Does not requires the call to InitiateMajorParsing
	 * @param testsuffix
	 * @return
	 * @throws Exception
	 */
	public ArrayList<SupplierDef> parseFileForSuppliers(String testsuffix) throws Exception {
		BufferedReader reader = new BufferedReader(new FileReader(cadenasfile));
		ArrayList<SupplierDef> supplierlist=new ArrayList<SupplierDef>();
		HashMap<String,String> registeredncage=new HashMap<String,String>();
		String dummystring="0";
		String line = reader.readLine();
		if (testsuffix==null) testsuffix="";
		// discards first title line
		while ((line=reader.readLine())!=null) {
			if(!line.trim().equals("")){
				//if (line.length()!=CADENAS_CHAR_END) throw new Exception ("input file corrupted, incorrect size = "+line.length()+", line number = "+linenr);
				String rawncage=StringUtils.optimizedSubstring(line,F04_FABCLEREC_STID,F05_FABNOM_STID-1).trim()+testsuffix;
				if (registeredncage.get(rawncage)==null) {
					SupplierDef thissupplier=new SupplierDef(rawncage);
					registeredncage.put(rawncage,dummystring);
					supplierlist.add(thissupplier);
			}
			}	
		}
		reader.close();
		return supplierlist;
	}
}
