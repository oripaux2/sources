
package ext.eurocopter.x4.loaders.ecloader;

import java.io.*;
import java.net.*;
import org.apache.commons.lang.SystemUtils;

public class ECClassLoader extends ClassLoader
{

    public ECClassLoader(ClassLoader parent)
    {
        super(parent);
    }

    public Class loadClass(String name)
        throws ClassNotFoundException
    {
        if(name.indexOf("ext.eurocopter.x4.loaders.ecloader.StandardLoader") == -1)
            return super.loadClass(name);
        String fileName = name.substring(name.lastIndexOf(".") + 1);
        try
        {
            String pathReloadingClass = "";
            if(SystemUtils.IS_OS_AIX || SystemUtils.IS_OS_LINUX || SystemUtils.IS_OS_UNIX)
                pathReloadingClass = "/home/wtadm/scriptclass/";
            else
            if(SystemUtils.IS_OS_WINDOWS)
                pathReloadingClass = "C:\\scriptclass\\";
            String url = (new StringBuilder("file:")).append(pathReloadingClass).append(fileName).append(".class").toString();
            URL myUrl = new URL(url);
            URLConnection connection = myUrl.openConnection();
            InputStream input = connection.getInputStream();
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            for(int data = input.read(); data != -1; data = input.read())
                buffer.write(data);

            input.close();
            byte classData[] = buffer.toByteArray();
            return defineClass(name, classData, 0, classData.length);
        }
        catch(MalformedURLException e)
        {
            e.printStackTrace();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
}