package ext.eurocopter.x4.loaders.ecloader;

import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class ECConsoleFormatter extends Formatter {

	private String lineseparator;

	public ECConsoleFormatter() {
		lineseparator = System.getProperty("line.separator");
	}

	@Override
	public String format(LogRecord log) {
		// TODO Auto-generated method stub
		StringBuffer sb = new StringBuffer();

		sb.append(log.getMessage());
		sb.append(lineseparator);

		return sb.toString();
	}

}
