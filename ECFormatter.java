package ext.eurocopter.x4.loaders.ecloader;

import java.text.SimpleDateFormat;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class ECFormatter extends Formatter {

		private SimpleDateFormat format;
		private String lineseparator;
		public ECFormatter() {
			format=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
			lineseparator=System.getProperty("line.separator");
		}
		@Override
		public String format(LogRecord log) {
			// TODO Auto-generated method stub
			StringBuffer sb=new StringBuffer();
			sb.append(log.getLevel().getName());
			
			completeToLength(sb,8);
			
			sb.append(log.getThreadID());
			completeToLength(sb,13);
			sb.append(format.format(new java.util.Date(log.getMillis())));
			completeToLength(sb,38);
			sb.append(log.getSourceClassName());
			sb.append(".");
			sb.append(log.getSourceMethodName());
			completeToLength(sb,115);

			sb.append(log.getMessage());
			sb.append(lineseparator);
			
			return sb.toString();
		}
		private void completeToLength(StringBuffer sb,int length) {
			char space=' ';
			while (sb.length()<length) sb.append(space);
		}

}
