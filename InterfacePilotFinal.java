package ext.eurocopter.x4.loaders.ecloader;

import java.io.File;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import wt.method.RemoteMethodServer;
import wt.util.WTProperties;



public class InterfacePilotFinal {
	public static Logger LOGGER = Logger.getAnonymousLogger();
	
	/**
	 * This class logs the error messages. 
	 * @param thismessage logs the error message
	 */
	public static void logReturnMessage(Msg thismessage) {
		if (thismessage.getResult()==Msg.RESULT_SUCCESS) {
			LOGGER.fine(thismessage.toString());
		}
		if (thismessage.getResult()==Msg.RESULT_WARNING) {
			LOGGER.warning(thismessage.toString());
			if(thismessage.getOriginalthrowable() != null)
				LOGGER.warning("WARNING CAUSE : "+thismessage.getOriginalthrowable().toString());
			logSeparator("BEGIN WARINING STACK TRACE");
			for (int j=0;j<thismessage.getOriginalthrowable().getStackTrace().length;j++) {
				LOGGER.info(thismessage.getOriginalthrowable().getStackTrace()[j].toString());
			}
			logSeparator("END WARINING STACK TRACE");
		}
		if (thismessage.getResult()==Msg.RESULT_ERROR) {
			LOGGER.severe(thismessage.toString());
			logSeparator("BEGIN ERROR STACK TRACE");
			for (int j=0;j<thismessage.getOriginalthrowable().getStackTrace().length;j++) {
				LOGGER.info(thismessage.getOriginalthrowable().getStackTrace()[j].toString());
			}
			logSeparator("END ERROR STACK TRACE");
		}
	}
	
	public static void logSeparator(String position){
		LOGGER.info("--------------------------------------------------------------------"+position+"----------------------------------------------------------------------");
	}
	/**
	 * Logs a batch of messages received by the component on the server
	 * @param messages
	 */
	public static void logReturnMessages(ArrayList<Msg> messages) {
		if (messages!=null) 
			for (int i=0;i<messages.size();i++) {
				Msg thismessage = messages.get(i);
				logReturnMessage(thismessage);

			}
	}
	/**
	 * this feature sends error messages back from the server to the pilot for logging. It also sends message using the 
	 * windchill mail sending feature
	 * @param emailTo the e-mail address to send errors to
	 * @param messages the list of error messages
	 * @param inputfilename the file on which the error was detected, sent in the e-mail for more clarity
	 */
	public static void sendErrorMessages(String emailTo, ArrayList<Msg> messages, String inputfilename) {
		try {
			if (messages!=null) { 
				InternetAddress to = new InternetAddress(emailTo, null);
				String body = "";
				for (int i=0;i<messages.size();i++) {
					Msg thismessage = messages.get(i);
					if (thismessage.getResult()==Msg.RESULT_PROCESS_ERROR) {
						body += thismessage.getMessage()+"<br/>";
						//System.out.println(thismessage.getMessage());
					}
				}
				if (!"".equals(body)) {
					// Initialize email session
					Properties props = System.getProperties();
					props.put("mail.smtp.host", WTProperties.getServerProperties().getProperty("wt.mail.mailhost"));
					Session mailSession = Session.getDefaultInstance(props, null);
					// Message
					Message msg = new MimeMessage(mailSession);
					msg.setRecipient(Message.RecipientType.TO, to); 
					msg.setSubject("SAP - X4 Data Interface Errors"); 
					// Body
					msg.setContent("Errors occured during ECLoader processing, file: "+inputfilename+"<br/><br/>"+body, "text/html");
					Transport.send(msg);
				}
			}
		} catch (Throwable t) {
			System.out.println("unexpected error during execution, "+t.getMessage());
			LOGGER.severe("unexpected error during execution "+t.getMessage());
		}	
	}
	
	@SuppressWarnings("unchecked")
	public static void main(String args[]) {
		System.out.println("---------------------------------------------------------------------------------");
		System.out.println("+++ EC Loader for CorePDM standard ");
		System.out.println("+++ Version 0.2.14 ");
//		System.out.println("    version = 0.2.1 - 04 APR 2012- update by Jerome Larzilliere");
//		System.out.println("    version = 0.2.2 - 16 SEP 2012- update by Nicolas de Mauroy");
//		System.out.println("        * improvements: log on shared file issue, correct logging ");
//		System.out.println("        * following first go-live analysis on n-cage already existing");
//		System.out.println("    version = 0.2.3 - 15 APR 2013- update by Anthony Bucciero");
//		System.out.println("        * set view to 'Design' on created Standard");
//		System.out.println("    version = 0.2.4 - 03 MAY 2013- update by Anthony Bucciero");
//		System.out.println("        * fill new field didicated to Weight Maturity");
//		System.out.println("    version = 0.2.5 - 01 JUL 2013- update by Anthony Bucciero");
//		System.out.println("        * Automatically share new Std Part to the Daher project");
//		System.out.println("    version = 0.2.6 - 01 JAN 2014- update by Khalid Kajjoua and Eric Schoukroun");
//		System.out.println("        * resolve bug Java on substring method");
//		System.out.println("    version = 0.2.7 - 01 JAN 2014- update by Pierre Davoine");
//		System.out.println("        * change libelle of WEIGHTMATURITY");
//		System.out.println("    version = 0.2.8 - 21 NOV 2014- update by Eric Schoukroun");
//		System.out.println("        * manages not required iterations");
//		System.out.println("        * allows to increase column number in input file");
		System.out.println("---------------------------------------------------------------------------------");
		if (args.length<7) {

			System.out.println("Inccorect arguments -- see syntax below"); 
			System.out.println("windchill InterfacePilotFinal wcuser wcpassword inputfile methodserverurl ");
			System.out.println("         classprefix emailaddress logfilefolder newFormat [teststring]");
			System.out.println("where wcuser is the 'wcadmin' user name on the local windchill ");
			System.out.println("         instance");
			System.out.println("      password is the 'wcadmin' user password on the local windchill");
			System.out.println("         instance");
			System.out.println("      inputfile is the path (related to WT_HOME or absolute) of the input file from CADENAS");
			System.out.println("      methodserverurl is the Windchill url ");
			System.out.println("         e.g. 'http://myserver/Windchill/'");
			System.out.println("      classprefix is the StandardPart and SAPStandardMaterial prefix ");
			System.out.println("         in Type Manager of the instance (e.g. 'corp.eurocopter.eu.' )");
			System.out.println("         Warning: do NOT forget the final '.'");
			System.out.println("      emailaddress is the email address where errors log are sent.");
			System.out.println("      logfilefolder is the folder path of the log file (related to WT_HOME or absolute)"); // added in 0.2.2
			System.out.println("      teststring (optional) is a suffix that will be added for test");
			System.out.println("         purpose on supplier and part numbers (MATNR and NCAGE)");
			System.exit(1);
		}
		String wcuser=args[0];
		String wcpassword=args[1];
		String inputfilename=args[2];
		String methodserverurl=args[3];
		String classprefix=args[4];
		String emailaddress=args[5];
		String logfolferpath=args[6]; // added in 0.2.2
		Boolean isDevEnvironment = new Boolean(Arrays.asList(args).contains("isDevEnvironment"));
		for(int i=0; i<args.length; i++) {
			System.out.println("args["+i+"]="+args[i]);
		}
		System.out.println("isDevEnvironment = " + isDevEnvironment);
		File inputfile=new File(inputfilename);
		CadenasFileParser fileparser = new CadenasFileParser(inputfile);
		String suffix=""; // fixed in 0.1.14 - was null and caused issues
		if (args.length==8 && !isDevEnvironment) suffix = args[7];
		else if(args.length==9 && isDevEnvironment) suffix = args[8];
		System.out.println("Starting ECLoader ");
		FileHandler logfilehandler=null;
		try {	
			SimpleDateFormat format =new SimpleDateFormat("yyyy-MM-dd'T'HH_mm_ss");
			TimeZone timeZone = TimeZone.getTimeZone("CET");
			format.setTimeZone(timeZone);
			
			String logfilepath = logfolferpath+File.separator+"ECLoader_"+format.format(new Date(System.currentTimeMillis()))+"_%g.log";
			logfilehandler=new FileHandler(logfilepath,10000000,1000,true); // changed in 0.2.2
			logfilehandler.setLevel(Level.ALL);
			logfilehandler.setFormatter(new ECFormatter());
			//LOGGER.setUseParentHandlers(false);
			for (int i=0;i<LOGGER.getHandlers().length;i++) {
				LOGGER.removeHandler(LOGGER.getHandlers()[i]);
			}
			
			LOGGER.addHandler(logfilehandler);
			ConsoleHandler consolehandler = new ConsoleHandler();
			consolehandler.setFormatter(new ECConsoleFormatter());
			consolehandler.setLevel(Level.INFO);
			LOGGER.addHandler(consolehandler);
			LOGGER.setUseParentHandlers(false);
			LOGGER.info("Starting executiong of Standard Loader script, inputfile = "+inputfilename+", user = "+wcuser+", testsuffix = "+suffix);
			LOGGER.info("start processing at "+new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date()));
			// Mémoire totale allouée
			long totalMemory = Runtime.getRuntime().totalMemory();
			// Mémoire utilisée
			long currentMemory = totalMemory-Runtime.getRuntime().freeMemory();
			LOGGER.info("Starting - Total Memory : " + totalMemory);
			LOGGER.info("Starting - Current Memory : " + currentMemory);
			System.out.println("Established logging, logfilepath = "+logfilepath);

			Locale.setDefault(Locale.ENGLISH);
			URL url = new URL(methodserverurl);
			RemoteMethodServer rms = RemoteMethodServer.getInstance(url);
			rms.setUserName(wcuser);
			rms.setPassword(wcpassword);
			rms.invoke("execute","ext.eurocopter.x4.loaders.ecloader.StandardLoaderInvocator",null,new Class[]{Boolean.class, String.class, Class[].class, Object[].class},
					new Object[]{isDevEnvironment, "myping",new Class[] {String.class},new Object[] {"session ping"}});
			LOGGER.info("connection to Remote Method Server was achieved ");

			LOGGER.info("STEP 0 - preparing the Loader Class");
			// Mémoire totale allouée
			totalMemory = Runtime.getRuntime().totalMemory();
			// Mémoire utilisée
			currentMemory = totalMemory-Runtime.getRuntime().freeMemory();
			LOGGER.info("STEP 0 - Total Memory : " + totalMemory);
			LOGGER.info("STEP 0 - Current Memory : " + currentMemory);
			Msg stepzeroreturnmessage = (Msg) rms.invoke("execute","ext.eurocopter.x4.loaders.ecloader.StandardLoaderInvocator",null,new Class[]{Boolean.class, String.class, Class[].class, Object[].class},
					new Object[]{isDevEnvironment, "InitializeData", new Class[] {}, new Object[] {}});
			logReturnMessage(stepzeroreturnmessage);
			if (stepzeroreturnmessage.getResult()==Msg.RESULT_SUCCESS) {
				LOGGER.info("STEP 0 succesfully achieved");
				// Mémoire totale allouée
				totalMemory = Runtime.getRuntime().totalMemory();
				// Mémoire utilisée
				currentMemory = totalMemory-Runtime.getRuntime().freeMemory();
				LOGGER.info("STEP 0 - Total Memory : " + totalMemory);
				LOGGER.info("STEP 0 - Current Memory : " + currentMemory);
			} else {
				throw new Exception("Step 0 could not be achieved, stopping treatment");
			}
			LOGGER.info("STEP 1 - parsing file "+inputfile.getPath()+" for suppliers");
			// Mémoire totale allouée
			totalMemory = Runtime.getRuntime().totalMemory();
			// Mémoire utilisée
			currentMemory = totalMemory-Runtime.getRuntime().freeMemory();
			LOGGER.info("STEP 1 - Total Memory : " + totalMemory);
			LOGGER.info("STEP 1 - Current Memory : " + currentMemory);
			ArrayList<SupplierDef> retrievedsuppliers=fileparser.parseFileForSuppliers(suffix);
			LOGGER.info("STEP 1 - retrieved "+retrievedsuppliers.size()+" suppliers from the input file");
			// Mémoire totale allouée
			totalMemory = Runtime.getRuntime().totalMemory();
			// Mémoire utilisée
			currentMemory = totalMemory-Runtime.getRuntime().freeMemory();
			LOGGER.info("STEP 1 - Total Memory : " + totalMemory);
			LOGGER.info("STEP 1 - Current Memory : " + currentMemory);
			long timebeforesupplier=new Date().getTime();
			ArrayList<Msg> returnmessages = (ArrayList<Msg>) rms.invoke("execute","ext.eurocopter.x4.loaders.ecloader.StandardLoaderInvocator",null,new Class[]{Boolean.class, String.class, Class[].class, Object[].class},
						new Object[]{isDevEnvironment, "createSupplierBatch",new Class[]{retrievedsuppliers.getClass()},new Object[]{retrievedsuppliers}});
			long timeaftersupplier=new Date().getTime();
			float timeperorg = (timeaftersupplier-timebeforesupplier)/retrievedsuppliers.size();
			LOGGER.info("STEP 1 - finished executing supplier creation on Method Server");
			LOGGER.info("STEP 1 - total time = "+(timeaftersupplier-timebeforesupplier)+", ms per org = "+timeperorg);
			// Mémoire totale allouée
			totalMemory = Runtime.getRuntime().totalMemory();
			// Mémoire utilisée
			currentMemory = totalMemory-Runtime.getRuntime().freeMemory();
			LOGGER.info("STEP 1 - Total Memory : " + totalMemory);
			LOGGER.info("STEP 1 - Current Memory : " + currentMemory);
			logReturnMessages(returnmessages);

			int batchsize=250;
			int currentbatch=0;
			LOGGER.info("STEP 2 - Starting parsing batches, batchsize = "+batchsize);
			// Mémoire totale allouée
			totalMemory = Runtime.getRuntime().totalMemory();
			// Mémoire utilisée
			currentMemory = totalMemory-Runtime.getRuntime().freeMemory();
			LOGGER.info("STEP 2 - Total Memory : " + totalMemory);
			LOGGER.info("STEP 2 - Current Memory : " + currentMemory);
			fileparser.InitiateMajorparsing();
			ArrayList<StandardDef> refbatch = fileparser.parseNewStandardBatch(batchsize,suffix);
			while (refbatch!=null) {
				currentbatch++;
				LOGGER.info("STEP 2 Processing batch N"+currentbatch);
				long timestartbatch = new Date().getTime();
				ArrayList<Msg> returnpartmessages = (ArrayList<Msg>) rms.invoke("execute","ext.eurocopter.x4.loaders.ecloader.StandardLoaderInvocator",null,new Class[]{Boolean.class, String.class, Class[].class, Object[].class},
						new Object[]{isDevEnvironment, "createStandardBatch",new Class[]{refbatch.getClass(),String.class},new Object[]{refbatch,classprefix}});
				long timeendbatch = new Date().getTime();
				float timeperpart = (timeendbatch-timestartbatch)/refbatch.size();
				LOGGER.info("STEP 2 Processing batch N"+currentbatch+", time (ms) = "+(timeendbatch-timestartbatch)+", time per part (ms) = "+timeperpart);
				// Mémoire totale allouée
				totalMemory = Runtime.getRuntime().totalMemory();
				// Mémoire utilisée
				currentMemory = totalMemory-Runtime.getRuntime().freeMemory();
				LOGGER.info("STEP 2 - Total Memory : " + totalMemory);
				LOGGER.info("STEP 2 - Current Memory : " + currentMemory);
				logReturnMessages(returnpartmessages);
				sendErrorMessages(emailaddress, returnpartmessages, inputfilename);
				if (refbatch.size()<batchsize) {
					refbatch=null;
				} else {
					refbatch = fileparser.parseNewStandardBatch(batchsize,suffix);
				}
			}
			fileparser.CloseMajorParsing();
			LOGGER.info("STEP 3 - Create Alternate Link");
			// Mémoire totale allouée
			totalMemory = Runtime.getRuntime().totalMemory();
			// Mémoire utilisée
			currentMemory = totalMemory-Runtime.getRuntime().freeMemory();
			LOGGER.info("STEP 3 - Total Memory : " + totalMemory);
			LOGGER.info("STEP 3 - Current Memory : " + currentMemory);
			ArrayList<AlternateDef> alternatelist = fileparser.parseFileForAlternates(suffix);
			LOGGER.info("found alternates, number = "+alternatelist.size());
			ArrayList<Msg> returnalternatemessages = (ArrayList<Msg>) rms.invoke("execute","ext.eurocopter.x4.loaders.ecloader.StandardLoaderInvocator",null,new Class[]{Boolean.class, String.class, Class[].class, Object[].class},
					new Object[]{isDevEnvironment, "createAlternateBatch",new Class[]{alternatelist.getClass()},new Object[]{alternatelist}});
			logReturnMessages(returnalternatemessages);
			sendErrorMessages(emailaddress, returnalternatemessages, inputfilename);
			LOGGER.info("STEP 3 - Create Alternate Link - end");
			// Mémoire totale allouée
			totalMemory = Runtime.getRuntime().totalMemory();
			// Mémoire utilisée
			currentMemory = totalMemory-Runtime.getRuntime().freeMemory();
			LOGGER.info("STEP 3 - Total Memory : " + totalMemory);
			LOGGER.info("STEP 3 - Current Memory : " + currentMemory);
			LOGGER.info("end processing at "+new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date()));
			LOGGER.info("Standard loading operation is now terminated. Please transmit the log file to the business users for analysis.");
			
		} catch (Throwable T) {
			LOGGER.info("Standard loading operation is now terminated with error. Please transmit the log file to the business users for analysis.");
			System.out.println("unexpected error during execution "+T.getMessage());
			LOGGER.severe("unexpected error during execution "+T.getMessage());
			for (int i=0;i<T.getStackTrace().length;i++) {
				LOGGER.warning("detailed stacktrace: "+T.getStackTrace()[i].toString());
			}
			if (logfilehandler!=null) logfilehandler.close();
		}
		
		logfilehandler.close();

	}
}
