package ext.eurocopter.x4.loaders.ecloader;

import java.io.Serializable;

/**
 * @author Nicolas de Mauroy
 * An error message for the EC Loader tool
 */
public class Msg implements Serializable {
	private static final long serialVersionUID = -7803554661026718801L;
	public static int RESULT_SUCCESS=1;
	public static int RESULT_ERROR=-1;
	public static int RESULT_WARNING=0;
	public static int RESULT_PROCESS_ERROR=2;
	private int result;
	public static String OPERATION_INIT="Initialization";
	public static String OPERATION_SUPORG="Creation of supplier organization";
	public static String OPERATION_SUPMAN="Creation of supplier manufacturer";
	public static String OPERATION_PARTINIT="Initialization of Part Creation";
	public static String OPERATION_SAPMAT="Creation of SAP standard material";
	public static String OPERATION_SAPMATORGQUERY="Query on org to create standard part";
	public static String OPERATION_STDPART="Creation of standard part (SUMA)";
	public static String OPERATION_MANUFBY="Creation of the manufacturerByLink";
	public static String OPERATION_ALTERNATE="Creation of the alternatelink";
	private String operation;
	private String objectid;
	private String message;
	private Throwable originalthrowable;
	
	/**
	 * @param result result of the operation
	 * @param operation type of operation
	 * @param objectid id of the object for the operation
	 * @param message the message if an error or a warning is encountered
	 * @param originalthrowable the exception received
	 */
	public Msg(int result, String operation, String objectid,
			String message, Throwable originalthrowable) {
		super();
		this.result = result;
		this.operation = operation;
		this.objectid = objectid;
		this.message = message;
		this.originalthrowable = originalthrowable;
	}

	@Override
	public String toString() {
		if ((result==RESULT_ERROR) || (result==RESULT_WARNING)) {
			String results="[ECLOADER_ERROR]";
			if (result==RESULT_WARNING) results="[ECLOADER_WARNING]";
			return results+", op="+operation+", obj="+objectid+", msg="+this.message;
		}
		else if (result==RESULT_PROCESS_ERROR)
			return "[ECLOADER_ERROR], op="+operation+", obj="+objectid+", msg="+this.message;
		
		return "[ECLOADER_SUCCESS]"+", op="+operation+", obj="+objectid;
		
	}

	public int getResult() {
		return result;
	}

	public String getOperation() {
		return operation;
	}

	public String getObjectid() {
		return objectid;
	}

	public String getMessage() {
		return message;
	}

	public Throwable getOriginalthrowable() {
		return originalthrowable;
	}
	
}
