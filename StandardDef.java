package ext.eurocopter.x4.loaders.ecloader;

import java.io.Serializable;

public class StandardDef implements Serializable {
	private static final long serialVersionUID = -2409093739171760325L;
	private String matnr;
	private String ncage;
	private String mpn;
	private String englishdescription;
	private String germandescription;
	private String frenchdescription;
	private String spanishdescription;
	private String standardreferencenumber;
	private String hazMatInd;
	
	// Restricted values exist
	
	public static final String STATUSREF_REPLACEDSTANDARDPART="REPLACEDSTANDARDPART";
	public static final String STATUSREF_STANDARDPART="STANDARDPART";
	public static final String STATUSREF_NON_AERO_STD_PART="NONAEROSTANDARDPART";
	
	private String statusofthereference;
	
	// Restricted values exist
	public static final String SDRPERIMETER_ECG="ECG";
	public static final String SDRPERIMETER_ECD="ECD";
	public static final String SDRPERIMETER_EC="EC";
	
	private String qualifiedinsideSDRPerimeter;
	
	// Restricted values exist
	
	public static final String LIFECYCLE_SERIALRELEASED="SERIALRELEASED";
	public static final String LIFECYCLE_NOTRELEASEDFORNEWSTUDY="NOTRELEASEDFORNEWSTUDY";
	public static final String LIFECYCLE_CANCELLED="CANCELLED";
	public static final String LIFECYCLE_PRELIMINARY="PRELIMINARYSTATE";
	
	private String lifecycle;
	
	private String weight;
	
	// Restricted values exist
	
	public static final String WEIGHTMATURITY_CALCULATED="Calculated";
	public static final String WEIGHTMATURITY_ESTIMATED="Estimated";
	public static final String WEIGHTMATURITY_SUPPORTED="Supported";
	public static final String WEIGHTMATURITY_WEIGHTED="Weighted";
	//public static final String WEIGHTMATURITY_WEIGHTED="Weighed";
	
	
	private String weightmaturity;
	
	// Restricted values exist
	
	public static final String WEIGHTUNIT_GPC="GPC";
	public static final String WEIGHTUNIT_GM="GM";
	public static final String WEIGHTUNIT_GM2="GM2";
	public static final String WEIGHTUNIT_GL="GL";
	public static final String WEIGHTUNIT_G="G";
	private String weightunit;
	private String cms;
	
	private String partClassification;
	
	// Restricted values exist
	
	public static final String PART_CLASSIFICATION_GSE_AGE_TOOLS = "090";
	
	private String standardPartSoftype;
	
	// Restricted values exist
	
	public static final String STANDARD_SOFTTYPE_STD="StandardPart";
	public static final String STANDARD_SOFTTYPE_NON_AERO="NonAeroStandardPart";
	
	// Restricted values exist
	
	public static final String HAZ_MAT_IND_UNKNOW="Unknown";
	public static final String HAZ_MAT_IND_NOT_COMP="Not compliant";
	public static final String HAZ_MAT_IND_COMP="Compliant";
	public static final String HAZ_MAT_IND_PART_COMP="Partially compliant";
	
	
	// Restricted values exist
	
	private String activatedECDprod;
	
	// Restricted values exist
	
	
	private String activatedECprod;
	private boolean replacedstandardpart;
	private String[] additionalattributes;
	private String[] standardprogramvalidity;
	public StandardDef(String matnr, String ncage, String mpn,
			String englishdescription, String germandescription,
			String frenchdescription,String spanishdescription,
			String standardreferencenumber, String statusofthereference,
			String qualifiedinsideSDRPerimeter, String lifecycle,
			String weight, String weightmaturity, String weightunit,
			String cms, String activatedECDprod, String activatedECprod, 
			String standardPartSoftype, String partClassification,
			boolean replacedstandardpart, String[] additionalattributes,
			String[] standardprogramvalidity,String hazMatInd) {
		super();
		this.matnr = matnr;
		this.ncage = ncage;
		this.mpn = mpn;
		this.englishdescription = englishdescription;
		this.germandescription = germandescription;
		this.frenchdescription = frenchdescription;
		this.spanishdescription = spanishdescription;
		this.standardreferencenumber = standardreferencenumber;
		this.statusofthereference = statusofthereference;
		this.qualifiedinsideSDRPerimeter = qualifiedinsideSDRPerimeter;
		this.lifecycle = lifecycle;
		this.weight = weight;
		this.weightmaturity = weightmaturity;
		this.weightunit = weightunit;
		this.cms = cms;
		this.activatedECDprod = activatedECDprod;
		this.activatedECprod = activatedECprod;
		this.partClassification = partClassification;
		this.standardPartSoftype = standardPartSoftype;
		this.replacedstandardpart = replacedstandardpart;
		this.additionalattributes = additionalattributes;
		this.standardprogramvalidity = standardprogramvalidity;
		this.hazMatInd = hazMatInd;
	}
	public String getMatnr() {
		return matnr;
	}
	public String getNcage() {
		return ncage;
	}
	public String getMpn() {
		return mpn;
	}
	public String getEnglishdescription() {
		return englishdescription;
	}
	public String getGermandescription() {
		return germandescription;
	}
	public String getFrenchdescription() {
		return frenchdescription;
	}
	
	public String getSpanishdescription() {
		return spanishdescription;
	}

	public String getStandardreferencenumber() {
		return standardreferencenumber;
	}

	public String getStatusofthereference() {
		return statusofthereference;
	}

	public String getQualifiedinsideSDRPerimeter() {
		return qualifiedinsideSDRPerimeter;
	}
	public static String getLIFECYCLE_SERIALRELEASED() {
		return LIFECYCLE_SERIALRELEASED;
	}
	
	public String getLifecycle() {
		return lifecycle;
	}
	public String getWeight() {
		return weight;
	}
	
	public String getWeightmaturity() {
		return weightmaturity;
	}
	public static String getWEIGHTUNIT_GPC() {
		return WEIGHTUNIT_GPC;
	}
	public static String getWEIGHTUNIT_GM() {
		return WEIGHTUNIT_GM;
	}
	public static String getWEIGHTUNIT_GM2() {
		return WEIGHTUNIT_GM2;
	}
	
	public String getWeightunit() {
		return weightunit;
	}
	public String getCms() {
		return cms;
	}
	public String getActivatedECDprod() {
		return activatedECDprod;
	}
	public String getActivatedECprod() {
		return activatedECprod;
	}
	public String getPartClassification() {
		return partClassification;
	}
	public String getStandardPartSoftype() {
		return standardPartSoftype;
	}
	public boolean isReplacedstandardpart() {
		return replacedstandardpart;
	}
	public String[] getAdditionalattributes() {
		return additionalattributes;
	}
	public String[] getStandardprogramvalidity() {
		return standardprogramvalidity;
	}
	
	public String getHazMatInd() {
		return hazMatInd;
	}
	
}
