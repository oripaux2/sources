package ext.eurocopter.x4.loaders.ecloader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;


import wt.configuration.TraceCode;
import wt.fc.Persistable;
import wt.fc.PersistenceHelper;
import wt.fc.PersistenceServerHelper;
import wt.fc.QueryResult;
import wt.fc.ReferenceFactory;
import wt.fc.collections.WTArrayList;
import wt.folder.Cabinet;
import wt.folder.Folder;
import wt.folder.FolderHelper;
import wt.inf.container.OrgContainer;
import wt.inf.container.WTContainer;
import wt.inf.container.WTContainerHelper;
import wt.inf.container.WTContainerRef;
import wt.inf.sharing.DataSharingHelper;
import wt.inf.sharing.SharedContainerMap;
import wt.lifecycle.LifeCycleHelper;
import wt.lifecycle.LifeCycleTemplate;
import wt.org.OrganizationServicesHelper;
import wt.org.WTOrganization;
import wt.org.WTOrganizationIdentifier;
import wt.part.QuantityUnit;
import wt.part.Source;
import wt.part.WTPart;
import wt.part.WTPartAlternateLink;
import wt.part.WTPartHelper;
import wt.part.WTPartMaster;
import wt.pds.StatementSpec;
import wt.pom.Transaction;
import wt.projmgmt.admin.Project2;
import wt.query.QuerySpec;
import wt.query.SearchCondition;
import wt.sandbox.SandboxHelper;
import wt.session.SessionServerHelper;
import wt.type.Typed;
import wt.util.WTAttributeNameIfc;
import wt.util.WTException;
import wt.util.WTPropertyVetoException;
import wt.vc.views.View;
import wt.vc.views.ViewHelper;
import wt.vc.views.ViewReference;
import wt.vc.wip.CheckoutLink;
import wt.vc.wip.ObjectsAlreadyCheckedOutException;
import wt.vc.wip.WorkInProgressException;
import wt.vc.wip.WorkInProgressHelper;
import wt.vc.wip.Workable;

import com.ptc.core.lwc.server.LWCNormalizedObject;
import com.ptc.core.meta.common.FloatingPoint;
import com.ptc.core.meta.common.TypeInstanceIdentifier;
import com.ptc.core.meta.common.UpdateOperationIdentifier;
import com.ptc.core.meta.type.mgmt.server.impl.WTTypeDefinition;
import com.ptc.core.meta.type.mgmt.server.impl.WTTypeDefinitionMaster;
import com.ptc.windchill.suma.axl.AXLContext;
import com.ptc.windchill.suma.axl.AXLHelper;
import com.ptc.windchill.suma.axl.AXLPreference;
import com.ptc.windchill.suma.axl.AXLServerHelper;
import com.ptc.windchill.suma.part.ManufacturerPart;
import com.ptc.windchill.suma.part.ManufacturerPartMaster;
import com.ptc.windchill.suma.supplier.Manufacturer;
import com.ptc.windchill.suma.supplier.Supplier;

import ext.eurocopter.utils.CollectionUtil;
import ext.eurocopter.utils.StringUtils;


/**
 * this class provides all methods for creation of update of standard parts.
 * @author Nicolas de Mauroy / Jerome Larzilliere / Anthony Bucciero
 *
 */
public class StandardLoader {
	private static String TRANSVERSAL_COMPONENTS_ORG = "transversal components";
	private static String STANDARD_PARTS_LIBRARY = "Standard Parts";
	private static String DESIGN_VIEW = "Design View";
	private static String DAHER_PROJECT = "Daher project";
	
	
	private static String EC_STANDARD_PART_LC = "EC Standard Part LC";
	private static int[] qscrap = new int[0];
	private static WTOrganization tcorg;
	private static WTContainer standardlibrary;
	private static WTContainerRef standardlibraryref;
	private static OrgContainer orgcontainer;
	private static ViewReference designViewReference;
	private static WTContainerRef daherProjectContainerRef;
	private static Folder daherProjectStandardFolder;

	private static String ERROR1 = "Error1: The SAP Standard Material with the number=%s is checked-out. Please impact SAP data modification in X4 Windchill manually.";
	private static String ERROR2 = "Error2: The Standard Part with the number=%s is checked-out. Please impact SAP data modification in X4 Windchill manually. The SAP Standard Material is no more synchronized with its Standard Part.";
	private static String ERROR3 = "Error3: The Standard Part with the number=%s is used by more than one MATNR SAP Side. Please correct SAP manually.";
	private static String ERROR4 = "Error4: The SAP Standard Material with the number=%s has 2 different MPN defined in BD1 as TypePN=S. Please correct SAP manually by removing one of them.";
	private static String ERROR5 = "Error5: Unable to share the Std Part. Please consult logs for more details";
	
	/**
	 * 
	 * @param pingref - a reference for the ping that will be displayed in Method Server log
	 * @throws WTException
	 * @throws WTPropertyVetoException
	 */
	public static void myping(String pingref) throws WTException, WTPropertyVetoException {
		System.out.println("Method Server Advanced ping start ref = " + pingref);
		//------------------------- GET TC ORGANIZATION -------------------------------
		QuerySpec qs = new wt.query.QuerySpec(WTOrganization.class);
		qs.appendWhere(new SearchCondition(WTOrganization.class, WTOrganization.NAME, "=", TRANSVERSAL_COMPONENTS_ORG), new int[] { 0 });
		QueryResult qr = PersistenceHelper.manager.find((StatementSpec) qs);
		tcorg = (WTOrganization) qr.nextElement();
		System.out.println("- PING CHECK - Locked up tc organization, id = " + tcorg.getPrincipalDisplayIdentifier());

		// ------------------------- LOOK UP MANUALLY CREATED ORGANIZATION -----------------

		//------------------------- GET STANDARD CONTAINER ----------------------------

		qs = new QuerySpec(WTContainer.class);
		qs.appendWhere(new SearchCondition(WTContainer.class, WTContainer.NAME, "=", "Standard Parts"), new int[] { 0 });
		qs.appendAnd();
		qs.appendWhere(new SearchCondition(WTContainer.class, WTContainer.ORGANIZATION_REFERENCE + '.' + WTAttributeNameIfc.REF_OBJECT_ID, SearchCondition.EQUAL, PersistenceHelper
				.getObjectIdentifier(tcorg).getId()), new int[] { 0 });
		qr = PersistenceHelper.manager.find((StatementSpec) qs);

		WTContainer standardlibrary = (WTContainer) qr.nextElement();
		System.out.println("- PING CHECK - Locked up tc standard library, id = " + standardlibrary.getName());
		System.out.println("- PING CHECK - OVER" + pingref);
	}

	/**
	 * This method should be called before any call to data loading methods,
	 * as it will pre-load environment objects
	 */

	public static Msg InitializeData() {

		// gets the organization for standards
		try {
			QuerySpec qsorg = new wt.query.QuerySpec(WTOrganization.class);
			qsorg.appendWhere(new SearchCondition(WTOrganization.class, WTOrganization.NAME, "=", TRANSVERSAL_COMPONENTS_ORG), new int[] { 0 });
			QueryResult qrorg = PersistenceHelper.manager.find((StatementSpec) qsorg);
			tcorg = (WTOrganization) qrorg.nextElement();
			orgcontainer = (OrgContainer) WTContainerHelper.service.getByPath("/wt.inf.container.OrgContainer=" + "transversal components").getObject();
			System.out.println("INIT01 - retrieved organization ");
		} catch (Throwable th) {
			// if error, no need to continue
			//th.printStackTrace();
			return new Msg(Msg.RESULT_ERROR, Msg.OPERATION_INIT, TRANSVERSAL_COMPONENTS_ORG, "the transversal component organization could not be found", th);
		}
		// get the standard library
		try {
			QuerySpec qslib = new QuerySpec(WTContainer.class);
			qslib.appendWhere(new SearchCondition(WTContainer.class, WTContainer.NAME, "=", STANDARD_PARTS_LIBRARY), qscrap);
			qslib.appendAnd();
			qslib.appendWhere(new SearchCondition(WTContainer.class, WTContainer.ORGANIZATION_REFERENCE + '.' + WTAttributeNameIfc.REF_OBJECT_ID, SearchCondition.EQUAL,
					PersistenceHelper.getObjectIdentifier(tcorg).getId()), qscrap);
			QueryResult qrlib = PersistenceHelper.manager.find((StatementSpec) qslib);
			standardlibrary = (WTContainer) qrlib.nextElement();
			standardlibraryref = standardlibrary.getContainerReference();
			System.out.println("INIT02 - retrieved library ");
		} catch (Throwable th) {
			//th.printStackTrace();
			return new Msg(Msg.RESULT_ERROR, Msg.OPERATION_INIT, STANDARD_PARTS_LIBRARY, "the standard library could not be found", th);
		}
		//
		// get the View Design reference
		try {
			QuerySpec querySpec = new QuerySpec(View.class);
			SearchCondition cond = new SearchCondition(View.class, View.NAME, SearchCondition.EQUAL, "Design");
			querySpec.appendWhere(cond, new int[] { 0 });
			//
			QueryResult queryResult = PersistenceHelper.manager.find((StatementSpec) querySpec);
			if (!queryResult.hasMoreElements()) {
				throw new WTException("Query returns no result: " + querySpec.toString());
			} else {
				designViewReference = ViewReference.newViewReference(((View) queryResult.nextElement()));
			}
		} catch (Throwable th) {
			return new Msg(Msg.RESULT_ERROR, Msg.OPERATION_INIT, DESIGN_VIEW, "could not retrieve the View Design", th);
		}
		//
		// retrieve the Daher project WTContainerRef
		try {
			QuerySpec querySpec = new QuerySpec(Project2.class);
			querySpec.appendWhere(new SearchCondition(Project2.class, Project2.NAME, SearchCondition.EQUAL, "Daher", false), new int[] { 0 });
			QueryResult queryResult = PersistenceHelper.manager.find((StatementSpec) querySpec);
			if (!queryResult.hasMoreElements()) {
				throw new WTException("Query returns no result: " + querySpec.toString());
			} else {
				daherProjectContainerRef = WTContainerRef.newWTContainerRef((Project2) queryResult.nextElement());
			}
		} catch (Throwable th) {
			return new Msg(Msg.RESULT_ERROR, Msg.OPERATION_INIT, DAHER_PROJECT, "could not retrieve the Daher project", th);
		}
		//
		// retrieve the daher project folder where the Std Part are shared  
		try {
			Cabinet cab = daherProjectContainerRef.getReferencedContainer().getDefaultCabinet();
			String folderStr = "/" + cab.getName() + "/Standards";
			daherProjectStandardFolder = FolderHelper.service.getFolder(folderStr, daherProjectContainerRef);
		} catch (Throwable th) {
			return new Msg(Msg.RESULT_ERROR, Msg.OPERATION_INIT, DAHER_PROJECT, "could not retrieve the Daher project folder dedicated to the shared Standard", th);
		}
		
		
		return new Msg(Msg.RESULT_SUCCESS, Msg.OPERATION_INIT, "all", null, null);
	}

	/**
	 * Creates the suppliers. If the creation of a supplier fails, a check is made, and no error is returned if the supplier exists
	 * in the database. This is a (quite dirty) way to manage both create and update.
	 * @param supplierlist the list of suppliers to be created
	 * @return messages encountered during execution
	 */
	public static ArrayList<Msg> createSupplierBatch(ArrayList<SupplierDef> supplierlist) {
		ArrayList<Msg> msglist = new ArrayList<Msg>();
		System.err.println("[ECLOADER START CREATE SUPPLIER BATCH]");
		boolean enforced = SessionServerHelper.manager.setAccessEnforced(false);
		if (supplierlist != null)
		{
			int size = supplierlist.size();
			for (int i = 0; i < size; i++) {
				SupplierDef thissupplier = supplierlist.get(i);
				WTOrganization supplierorg = null;

				// ------------------------------------------
				// CREATION OF ORGANIZATION FOR SUPPLIER
				// ------------------------------------------
				try {

					// Create the organisation id
					WTOrganizationIdentifier orgId = wt.org.WTOrganizationIdentifier.newWTOrganizationIdentifier();
					orgId.setCodingSystem("0141");
					orgId.setUniqueIdentifier(thissupplier.getNcage());

					// Create the Organization
					supplierorg = WTOrganization.newWTOrganization(thissupplier.getNcage());
					supplierorg.setOrganizationIdentifier(orgId);
					supplierorg = (WTOrganization) OrganizationServicesHelper.manager.createPrincipal(supplierorg);
					msglist.add(new Msg(Msg.RESULT_SUCCESS, Msg.OPERATION_SUPORG, thissupplier.getNcage(), null, null));
				} catch (Throwable t) {
					//t.printStackTrace();
					// line below suppressed in suite 0.2.2
					// msglist.add(new Msg(Msg.RESULT_WARNING,Msg.OPERATION_SUPORG,thissupplier.getNcage(),"supplier org could not be created, will try to lock-it up",t));
					//System.err.println(msglist.get(msglist.size()-1));
					// try to lookup if could not create
					try {
						supplierorg = StandardLoaderTool.getsupplierOrganization(thissupplier.getNcage());
					} catch (Throwable t2) {
						//t2.printStackTrace();
						msglist.add(new Msg(Msg.RESULT_WARNING, Msg.OPERATION_SUPORG, thissupplier.getNcage(),
								"supplier org could not be created, could not look-it up, see error below", t));
						msglist.add(new Msg(Msg.RESULT_ERROR, Msg.OPERATION_SUPORG, thissupplier.getNcage(), "supplier org could not be looked-up", t2));
						//System.err.println(msglist.get(msglist.size()-1));
					} // end recovery
				} // end catch supplier error
				// ------------------------------------------
				// CREATION OF MANUFACTURER FOR SUPPLIER
				// ------------------------------------------
				try {
					Supplier supplier = Manufacturer.newManufacturer(supplierorg, orgcontainer);
					supplier = (Supplier) PersistenceHelper.manager.save(((wt.fc.Persistable) (supplier)));
					msglist.add(new Msg(Msg.RESULT_SUCCESS, Msg.OPERATION_SUPMAN, thissupplier.getNcage(), null, null));
				} catch (Throwable t) {
					//t.printStackTrace();
					try {
						Manufacturer man = StandardLoaderTool.getManufacturerOnOrganization(supplierorg, orgcontainer);
						if (man == null)
							throw new Exception("query on manufacturer returned a null value");
					} catch (Throwable t2) {

						msglist.add(new Msg(Msg.RESULT_WARNING, Msg.OPERATION_SUPORG, thissupplier.getNcage(), "supplier manufacturer could not be created", t));
						msglist.add(new Msg(Msg.RESULT_WARNING, Msg.OPERATION_SUPORG, thissupplier.getNcage(), "supplier manufacturer is not existing prior to creation ", t2));
						//System.err.println(msglist.get(msglist.size()-1));
					}

				}
			} // end loop on suppliers	
		}
		System.err.println("[ECLOADER END CREATE SUPPLIER BATCH]");
		SessionServerHelper.manager.setAccessEnforced(enforced);
		return msglist;
	}

	/**
	 * This method creates the StandardPart (MPN)
	 * @param std standard data
	 * @param lcTpl lifecycle template for the creation
	 * @param manuforg manufacturer organization
	 * @param typeprefix a prefix used only for test purposes
	 * @param returnmessages handle script execution messages (info, warnings, errors)
	 * @throws Exception
	 */
	private static void createStandardPart(StandardDef std, LifeCycleTemplate lcTpl, WTOrganization manuforg, String typeprefix, ArrayList<Msg> returnmessages) throws Exception {
		System.err.println("1-createStandardPart mpn=" + std.getMpn());
		LWCNormalizedObject lwcNormalizedObjmpn = new LWCNormalizedObject(typeprefix + std.getStandardPartSoftype(), null, null);
		lwcNormalizedObjmpn.load(
				"name", 
				"number", 
				"organization.id", 
				"container.id", 
				"containerReference", 
				"folder", 
				"domainRef", 
				"lifeCycleTemplate", 
				"defaultTraceCode",
				"endItem", 
				"source", 
				"partType", 
				"hidePartInStructure", 
				"defaultUnit", 
				"FrenchDescription", 
				"GermanDescription", 
				"SpanishDescription", 
				"SAPNumber", 
				"OldReference",
				"StandardReferenceNumber", 
				"StatusoftheReference", 
				"QualifiedInsideSDRPerimeter", 
				"Weight", 
				"WeightMaturity", 
				"WeightUnit", 
				"CMS", 
				"ReplacedStandardPart",
				"AdditionalAttributes", 
				"StandardProgramValidity", 
				"lifeCycleState", 
				"ActivatedECDProduction", 
				"ActivatedECProduction", 
				"view", 
				"PercentCalculatedWeight",
				"PercentEstimatedWeight", 
				"PercentWeightedWeight");

		lwcNormalizedObjmpn.set("name", std.getEnglishdescription());
		lwcNormalizedObjmpn.set("number", std.getMpn());
		lwcNormalizedObjmpn.set("organization.id", manuforg);
		lwcNormalizedObjmpn.set("container.id", standardlibrary);
		lwcNormalizedObjmpn.set("folder", "/Default/");
		lwcNormalizedObjmpn.set("SAPNumber", std.getMatnr());
		lwcNormalizedObjmpn.set("lifeCycleTemplate", lcTpl);
		lwcNormalizedObjmpn.set("defaultTraceCode", TraceCode.UNTRACED);
		lwcNormalizedObjmpn.set("endItem", false);
		lwcNormalizedObjmpn.set("source", Source.toSource("buy"));
		lwcNormalizedObjmpn.set("hidePartInStructure", false);
		lwcNormalizedObjmpn.set("defaultUnit", QuantityUnit.EA);
		lwcNormalizedObjmpn.set("FrenchDescription", std.getFrenchdescription());
		lwcNormalizedObjmpn.set("GermanDescription", std.getGermandescription());
		lwcNormalizedObjmpn.set("SpanishDescription", std.getSpanishdescription());
		lwcNormalizedObjmpn.set("partType", "component");
		lwcNormalizedObjmpn.set("StandardReferenceNumber", std.getStandardreferencenumber());
		lwcNormalizedObjmpn.set("StatusoftheReference", std.getStatusofthereference());
		lwcNormalizedObjmpn.set("QualifiedInsideSDRPerimeter", std.getQualifiedinsideSDRPerimeter());
		lwcNormalizedObjmpn.set("Weight", FloatingPoint.valueOf(std.getWeight()));
		lwcNormalizedObjmpn.set("WeightMaturity", std.getWeightmaturity());
		lwcNormalizedObjmpn.set("CMS", std.getCms());
		lwcNormalizedObjmpn.set("lifeCycleState", std.getLifecycle());
		lwcNormalizedObjmpn.set("ReplacedStandardPart", false);
		lwcNormalizedObjmpn.set("ActivatedECDProduction", std.getActivatedECDprod());
		lwcNormalizedObjmpn.set("ActivatedECProduction", std.getActivatedECprod());
		lwcNormalizedObjmpn.set("WeightUnit", std.getWeightunit());
		if (std.getAdditionalattributes() != null)
			if (std.getAdditionalattributes().length > 0) {
				lwcNormalizedObjmpn.set("AdditionalAttributes", std.getAdditionalattributes());
			}
		if (std.getStandardprogramvalidity() != null)
			if (std.getStandardprogramvalidity().length > 0) {
				lwcNormalizedObjmpn.set("StandardProgramValidity", std.getStandardprogramvalidity());
			}
		lwcNormalizedObjmpn.set("view", "Design");
		String currentWeightMaturity = std.getWeightmaturity();
		FloatingPoint percentCalculatedWeight = StandardDef.WEIGHTMATURITY_CALCULATED.equals(currentWeightMaturity) ? new FloatingPoint(100D, 1) : new FloatingPoint(0D, 1);
		FloatingPoint percentEstimatedWeight = StandardDef.WEIGHTMATURITY_ESTIMATED.equals(currentWeightMaturity) ? new FloatingPoint(100D, 1) : new FloatingPoint(0D, 1);
		FloatingPoint percentWeightedWeight = StandardDef.WEIGHTMATURITY_WEIGHTED.equals(currentWeightMaturity) ? new FloatingPoint(100D, 1) : new FloatingPoint(0D, 1);
		//
		lwcNormalizedObjmpn.set("PercentCalculatedWeight", percentCalculatedWeight);
		lwcNormalizedObjmpn.set("PercentEstimatedWeight", percentEstimatedWeight);
		lwcNormalizedObjmpn.set("PercentWeightedWeight", percentWeightedWeight);
		//
		TypeInstanceIdentifier stdPartIdentifier = lwcNormalizedObjmpn.persist();
		//
        //Fetch freshly created Id Card
		WTPart stdPart = (WTPart) new ReferenceFactory().getReference(stdPartIdentifier.getPersistenceIdentifier()).getObject();
		
		WTPartMaster master = (WTPartMaster)stdPart.getMaster();
		LWCNormalizedObject lwcObject = new LWCNormalizedObject(master,null,null,new UpdateOperationIdentifier());
		lwcObject.load("HazardousMaterialIndicator");
		lwcObject.set("HazardousMaterialIndicator", std.getHazMatInd());
		lwcObject.persist();
		//
		// automatically share new Std part to Daher project
		// if an error occurs manage it only here in order to do not disturb the global process
		try {
			//shareStdPartToDaherProject(getStdPart(std));
			shareStdPartToDaherProject(stdPart);
		} catch (Exception e) {
			Msg msg = new Msg(Msg.RESULT_WARNING, Msg.OPERATION_STDPART, std.getMatnr(), ERROR5, null);
			System.err.println(msg.toString());
			returnmessages.add(msg);
		}
	}

	
	private static boolean isStandardPartUpdateRequired(StandardDef std,
			LWCNormalizedObject lwcNormalizedObj,
			LWCNormalizedObject masterLwcRead,
			FloatingPoint percentCalculatedWeight,
			FloatingPoint percentEstimatedWeight,
			FloatingPoint percentWeightedWeight) throws WTException {
		boolean updateRequired;
		boolean updateRequiredForSinglesValues = true;
		boolean updateRequiredForMultiValuedValues = true;
		
		//System.out.println("isStandardPartUpdateRequired");
		
		CollectionUtil collectionUtil = new CollectionUtil();
		
		String lwcAdditionalAttributes = collectionUtil.getMultiValuedAsString(lwcNormalizedObj.get("AdditionalAttributes"));
		String lwcStandardProgramValidity = collectionUtil.getMultiValuedAsString(lwcNormalizedObj.get("StandardProgramValidity"));
		
		System.out.println("- StandardPart in database:" + nullToEmptyString((String)lwcNormalizedObj.get("SAPNumber")) + "|" +
		nullToEmptyString((String)lwcNormalizedObj.get("FrenchDescription")) + "|" +
		nullToEmptyString((String)lwcNormalizedObj.get("GermanDescription")) + "|" +
		nullToEmptyString((String)lwcNormalizedObj.get("SpanishDescription")) + "|" +
		nullToEmptyString((String)lwcNormalizedObj.get("StandardReferenceNumber")) + "|" +
		nullToEmptyString((String)lwcNormalizedObj.get("StatusoftheReference")) + "|" +
		nullToEmptyString((String)lwcNormalizedObj.get("QualifiedInsideSDRPerimeter")) + "|" +
		lwcNormalizedObj.get("Weight") + "|" +
		nullToEmptyString((String)lwcNormalizedObj.get("WeightMaturity")) + "|" +
		nullToEmptyString((String)lwcNormalizedObj.get("CMS")) + "|" +
		nullToEmptyString((String)lwcNormalizedObj.get("lifeCycleState")) + "|" +
		(Boolean)lwcNormalizedObj.get("ReplacedStandardPart") + "|" +
		nullToEmptyString((String)lwcNormalizedObj.get("ActivatedECDProduction")) + "|" +
		nullToEmptyString((String)lwcNormalizedObj.get("ActivatedECProduction")) + "|" +
		nullToEmptyString((String)lwcNormalizedObj.get("WeightUnit")) + "|" +
		lwcNormalizedObj.get("PercentCalculatedWeight") + "|" +
		lwcNormalizedObj.get("PercentEstimatedWeight") + "|" +
		lwcNormalizedObj.get("PercentWeightedWeight")+ "|" +
		lwcAdditionalAttributes + "|" +
		lwcStandardProgramValidity+ "|" +
		nullToEmptyString((String)masterLwcRead.get("HazardousMaterialIndicator")));
		
		String stdAdditionalAttributes = collectionUtil.getMultiValuedAsString(std.getAdditionalattributes());
		String stdStandardProgramValidity = collectionUtil.getMultiValuedAsString(std.getStandardprogramvalidity());
		
		
		System.out.println("- StandardPart in csv file:" + nullToEmptyString(std.getMatnr()) + "|" +
		nullToEmptyString(std.getFrenchdescription()) + "|" +
		nullToEmptyString(std.getGermandescription()) + "|" +
		nullToEmptyString(std.getSpanishdescription()) + "|" +
		nullToEmptyString(std.getStandardreferencenumber()) + "|" +
		nullToEmptyString(std.getStatusofthereference()) + "|" +
		nullToEmptyString(std.getQualifiedinsideSDRPerimeter()) + "|" +
		std.getWeight() + "|" +
		nullToEmptyString(std.getWeightmaturity()) + "|" +
		nullToEmptyString(std.getCms()) + "|" +
		nullToEmptyString(std.getLifecycle()) + "|" +
		std.isReplacedstandardpart() + "|" +
		nullToEmptyString(std.getActivatedECDprod()) + "|" +
		nullToEmptyString(std.getActivatedECprod()) + "|" +
		nullToEmptyString(std.getWeightunit()) + "|" +
		percentCalculatedWeight + "|" +
		percentEstimatedWeight + "|" +
		percentWeightedWeight + "|" +
		stdAdditionalAttributes + "|" +
		stdStandardProgramValidity +"|" +
		nullToEmptyString(std.getHazMatInd()));
		
		//System.out.println("isStandardPartUpdateRequired");
		
		//System.out.println("SAPNumber = " + lwcNormalizedObj.get("SAPNumber"));
		
		if(areEquals((String)lwcNormalizedObj.get("SAPNumber"),std.getMatnr()) &&
		areEquals((String)lwcNormalizedObj.get("FrenchDescription"),std.getFrenchdescription()) &&
		areEquals((String)lwcNormalizedObj.get("GermanDescription"),std.getGermandescription()) &&
		areEquals((String)lwcNormalizedObj.get("SpanishDescription"),std.getSpanishdescription()) &&
		areEquals((String)lwcNormalizedObj.get("StandardReferenceNumber"),std.getStandardreferencenumber()) &&
		areEquals((String)lwcNormalizedObj.get("StatusoftheReference"),std.getStatusofthereference()) &&
		areEquals((String)lwcNormalizedObj.get("QualifiedInsideSDRPerimeter"),std.getQualifiedinsideSDRPerimeter()) &&
		areEquals((FloatingPoint)lwcNormalizedObj.get("Weight"),FloatingPoint.valueOf(std.getWeight())) &&
		areEquals((String)lwcNormalizedObj.get("WeightMaturity"),std.getWeightmaturity()) &&
		areEquals((String)lwcNormalizedObj.get("CMS"),std.getCms()) &&
		areEquals((String)lwcNormalizedObj.get("lifeCycleState"),std.getLifecycle()) &&
		areEquals((Boolean)lwcNormalizedObj.get("ReplacedStandardPart"),new Boolean(std.isReplacedstandardpart())) &&
		areEquals((String)lwcNormalizedObj.get("ActivatedECDProduction"),std.getActivatedECDprod()) &&
		areEquals((String)lwcNormalizedObj.get("ActivatedECProduction"),std.getActivatedECprod()) &&
		areEquals((String)lwcNormalizedObj.get("WeightUnit"),std.getWeightunit()) &&
		areEquals((FloatingPoint)lwcNormalizedObj.get("PercentCalculatedWeight"),percentCalculatedWeight) &&
		areEquals((FloatingPoint)lwcNormalizedObj.get("PercentEstimatedWeight"),percentEstimatedWeight) &&
		areEquals((FloatingPoint)lwcNormalizedObj.get("PercentWeightedWeight"),percentWeightedWeight) &&
		areEquals((String)masterLwcRead.get("HazardousMaterialIndicator"), std.getHazMatInd())) {
		
			updateRequiredForSinglesValues = false;
		}
		//System.out.println("updateRequiredForSinglesValues = " + updateRequiredForSinglesValues);
		
		
		
		if (std.getAdditionalattributes() != null && std.getAdditionalattributes().length > 0) {
			
			//System.out.println("AdditionalAttributes");
			
			if(collectionUtil.equals(collectionUtil.getMultiValuedAsTable(lwcNormalizedObj.get("AdditionalAttributes"))
					,std.getAdditionalattributes())) {
				
				updateRequiredForMultiValuedValues = false;
				//System.out.println("Equals");
			}
		}
		else {
			updateRequiredForMultiValuedValues = false;
			//System.out.println("AdditionalAttributes Empty");
		}
		
		//System.out.println("updateRequiredForMultiValuedValues = " + updateRequiredForMultiValuedValues);
		
		if( ! updateRequiredForMultiValuedValues) {
			
			updateRequiredForMultiValuedValues = true;
			
			if (std.getStandardprogramvalidity() != null && std.getStandardprogramvalidity().length > 0) {
				
				//System.out.println("StandardProgramValidity");
				
				if(collectionUtil.equals(collectionUtil.getMultiValuedAsTable(lwcNormalizedObj.get("StandardProgramValidity"))
						,std.getStandardprogramvalidity())) {
					
					updateRequiredForMultiValuedValues = false;
					//System.out.println("Equals");
				}
			}
			else {
				updateRequiredForMultiValuedValues = false;
				//System.out.println("StandardProgramValidity Empty");
			}
		}
		
		//System.out.println("updateRequiredForMultiValuedValues = " + updateRequiredForMultiValuedValues);
		
		updateRequired = updateRequiredForSinglesValues || updateRequiredForMultiValuedValues;
		
		//System.out.println("updateRequired = " + updateRequired);
		
		
		return updateRequired;
	}
	
	
	/**
	 * This methods allows the creation of a standard part (Manufacturer Part)
	 * @param wtPart the WTPart to update
	 * @param std the data related to standard part
	 * @throws Exception any exception during process is thrown as-is
	 */
	private static void updateStandardPart(WTPart wtPart, StandardDef std) throws Exception {
		
		LWCNormalizedObject lwcNormalizedObjRead = new LWCNormalizedObject(wtPart, null, null, null);
		
		lwcNormalizedObjRead.load(
				"name", "number", "FrenchDescription", 
				"GermanDescription", "SpanishDescription", "SAPNumber", 
				"OldReference", "StandardReferenceNumber","StatusoftheReference", 
				"QualifiedInsideSDRPerimeter", 	"Weight", "WeightMaturity", 
				"WeightUnit", "CMS", "ReplacedStandardPart", "AdditionalAttributes",
				"StandardProgramValidity", "lifeCycleState",  "ActivatedECDProduction", 
				"ActivatedECProduction",  "PercentCalculatedWeight", "PercentEstimatedWeight",
				"PercentWeightedWeight");
		
		//
		LWCNormalizedObject masterLwcRead = new LWCNormalizedObject(wtPart.getMaster(), null, null, null);
		masterLwcRead.load("HazardousMaterialIndicator");
		String currentWeightMaturity = std.getWeightmaturity();
		FloatingPoint percentCalculatedWeight = StandardDef.WEIGHTMATURITY_CALCULATED.equals(currentWeightMaturity) ? new FloatingPoint(100D, 1) : new FloatingPoint(0D, 1);
		FloatingPoint percentEstimatedWeight = StandardDef.WEIGHTMATURITY_ESTIMATED.equals(currentWeightMaturity) ? new FloatingPoint(100D, 1) : new FloatingPoint(0D, 1);
		FloatingPoint percentWeightedWeight = StandardDef.WEIGHTMATURITY_WEIGHTED.equals(currentWeightMaturity) ? new FloatingPoint(100D, 1) : new FloatingPoint(0D, 1);
		
		
		boolean updateRequired = isStandardPartUpdateRequired(std, lwcNormalizedObjRead,masterLwcRead,percentCalculatedWeight, percentEstimatedWeight,percentWeightedWeight);
		
		if( ! updateRequired) {
			System.err.println("1-updateStandardPart not required wtpart=" + wtPart.getIdentity() + " matnr=" + std.getMatnr() + " mpn=" + std.getMpn());
			return;
		}
		
		System.err.println("1-updateStandardPart wtpart=" + wtPart.getIdentity() + " matnr=" + std.getMatnr() + " mpn=" + std.getMpn());
		
		boolean bool = SessionServerHelper.manager.isAccessEnforced();
		SessionServerHelper.manager.setAccessEnforced(false);
		/*Folder folder = WorkInProgressHelper.service.getCheckoutFolder();
		CheckoutLink checkoutLink = WorkInProgressHelper.service.checkout((Workable) persist, folder, null);
		persist = (WTPart) checkoutLink.getWorkingCopy();*/
		
		//Get working copy for update
		wtPart = (WTPart)getCheckedOut(wtPart, "");
		
		LWCNormalizedObject lwcNormalizedObj = new LWCNormalizedObject(wtPart, null, null, new UpdateOperationIdentifier());
		lwcNormalizedObj.load(
				"name", "number", "FrenchDescription", 
				"GermanDescription", "SpanishDescription", "SAPNumber", 
				"OldReference", "StandardReferenceNumber","StatusoftheReference", 
				"QualifiedInsideSDRPerimeter", 	"Weight", "WeightMaturity", 
				"WeightUnit", "CMS", "ReplacedStandardPart", "AdditionalAttributes",
				"StandardProgramValidity", "lifeCycleState",  "ActivatedECDProduction", 
				"ActivatedECProduction",  "PercentCalculatedWeight", "PercentEstimatedWeight",
				"PercentWeightedWeight");

		
		lwcNormalizedObj.set("SAPNumber", std.getMatnr());
		lwcNormalizedObj.set("FrenchDescription", std.getFrenchdescription());
		lwcNormalizedObj.set("GermanDescription", std.getGermandescription());
		lwcNormalizedObj.set("SpanishDescription", std.getSpanishdescription());
		lwcNormalizedObj.set("OldReference", "");
		lwcNormalizedObj.set("StandardReferenceNumber", std.getStandardreferencenumber());
		lwcNormalizedObj.set("StatusoftheReference", std.getStatusofthereference());
		lwcNormalizedObj.set("QualifiedInsideSDRPerimeter", std.getQualifiedinsideSDRPerimeter());
		lwcNormalizedObj.set("Weight", FloatingPoint.valueOf(std.getWeight()));
		lwcNormalizedObj.set("WeightMaturity", std.getWeightmaturity());
		lwcNormalizedObj.set("CMS", std.getCms());
		lwcNormalizedObj.set("lifeCycleState", std.getLifecycle());
		lwcNormalizedObj.set("ReplacedStandardPart", std.isReplacedstandardpart());
		lwcNormalizedObj.set("ActivatedECDProduction", std.getActivatedECDprod());
		lwcNormalizedObj.set("ActivatedECProduction", std.getActivatedECprod());
		lwcNormalizedObj.set("WeightUnit", std.getWeightunit());
		if (std.getAdditionalattributes() != null && std.getAdditionalattributes().length > 0) {
			lwcNormalizedObj.set("AdditionalAttributes", std.getAdditionalattributes());
		}
		if (std.getStandardprogramvalidity() != null && std.getStandardprogramvalidity().length > 0) {
			lwcNormalizedObj.set("StandardProgramValidity", std.getStandardprogramvalidity());
		}
		
		//
		lwcNormalizedObj.set("PercentCalculatedWeight", percentCalculatedWeight);
		lwcNormalizedObj.set("PercentEstimatedWeight", percentEstimatedWeight);
		lwcNormalizedObj.set("PercentWeightedWeight", percentWeightedWeight);
		//
		Persistable standardPart = lwcNormalizedObj.apply();
		standardPart = PersistenceHelper.manager.modify(standardPart);
		standardPart = WorkInProgressHelper.service.checkin((Workable) standardPart, null);
		
		WTPartMaster master = (WTPartMaster)((WTPart)standardPart).getMaster();
		LWCNormalizedObject lwcObject = new LWCNormalizedObject(master,null,null,new UpdateOperationIdentifier());
		lwcObject.load("HazardousMaterialIndicator");
		lwcObject.set("HazardousMaterialIndicator", std.getHazMatInd());
		lwcObject.persist();
		
		/*lwcNormalizedObj.apply();
		PersistenceHelper.manager.save(wtPart);
		WorkInProgressHelper.service.checkin((Workable) wtPart, null);*/
		// set the view Design
		ViewHelper.service.reassignView(wtPart.getMaster(), designViewReference);
		SessionServerHelper.manager.setAccessEnforced(bool);
	}

	

	/**
	 * puts the flag old ereference of a standard part to true;
	 * @param persist the "standard part" to update
	 * @throws Exception any exception caught is thrown
	 */
	private static void updateStandardPartOldReference(WTPart persist) throws Exception {
		System.err.println("2-updateStandardPartOldReference wtpart=" + persist.getIdentity());
		Folder folder = WorkInProgressHelper.service.getCheckoutFolder();
		CheckoutLink checkoutLink = WorkInProgressHelper.service.checkout((Workable) persist, folder, null);
		persist = (WTPart) checkoutLink.getWorkingCopy();
		LWCNormalizedObject lwcNormalizedObjmpn = new LWCNormalizedObject(persist, null, null, new UpdateOperationIdentifier());
		lwcNormalizedObjmpn.load("name", "number", "OldReference");

		lwcNormalizedObjmpn.set("OldReference", true);

		lwcNormalizedObjmpn.apply();
		PersistenceHelper.manager.save(persist);
		// set the view Design
		ViewHelper.service.reassignView(persist.getMaster(), designViewReference);
		WorkInProgressHelper.service.checkin((Workable) persist, null);
	}

	/**
	 * Create the SAP standard material from a standard definition. Note: the link to the standard part defined as 
	 * a SUMA MPN is not performed by this function
	 * @param std data of the standard
	 * @param lcTpl lifecycle template to be used
	 * @param typeprefix prefix used for test purposes. Should be an empty (but not null) String in production use;
	 * @throws Exception any exception caught is rethrown without any treatment
	 */
	private static void createSapMaterial(StandardDef std, LifeCycleTemplate lcTpl, String typeprefix) throws Exception {
		System.err.println("0-createSapMaterial matnr=" + std.getMatnr());
		LWCNormalizedObject lwcNormalizedObj = new LWCNormalizedObject(typeprefix + "SAPStandardMaterial", null, null);
		lwcNormalizedObj.load(
				"name", 
				"number", 
				"organization.id", 
				"container.id", 
				"folder", 
				"lifeCycleTemplate", 
				"defaultTraceCode", 
				"endItem", 
				"source", 
				"FrenchDescription",
				"GermanDescription", 
				"SpanishDescription", 
				"partType", 
				"hidePartInStructure", 
				"defaultUnit", 
				"usedQuantityUnit", 
				"StandardReferenceNumber", 
				"StatusoftheReference",
				"QualifiedInsideSDRPerimeter", 
				"Weight", 
				"WeightMaturity", 
				"WeightUnit", 
				"CMS", 
				"lifeCycleState",
				"PartClassification",
				"ReplacedStandardPart", 
				"ActivatedECDProduction",
				"ActivatedECProduction", 
				"AdditionalAttributes", 
				"StandardProgramValidity", 
				"view");

		// PREPARE ENGLISH DESCRIPTION FOR SAP MATNR
		String sapsm_engl = std.getMpn() + "\\" + std.getEnglishdescription();
		if (sapsm_engl.length() > 40)
			sapsm_engl = StringUtils.optimizedSubstring(sapsm_engl,0, 40);

		String sapsm_french = std.getMpn() + "\\" + std.getFrenchdescription();
		if (sapsm_french.length() > 40)
			sapsm_french = StringUtils.optimizedSubstring(sapsm_french,0, 40);

		String sapsm_german = std.getMpn() + "\\" + std.getGermandescription();
		if (sapsm_german.length() > 40)
			sapsm_german = StringUtils.optimizedSubstring(sapsm_german,0, 40);

		String sapsm_spanish = std.getMpn() + "\\" + std.getSpanishdescription();
		if (sapsm_spanish.length() > 40)
			sapsm_spanish = StringUtils.optimizedSubstring(sapsm_spanish,0, 40);
		System.out.println("-------------------name-------"+sapsm_engl);
		lwcNormalizedObj.set("name", sapsm_engl);
		lwcNormalizedObj.set("number", std.getMatnr());
		lwcNormalizedObj.set("organization.id", tcorg);
		lwcNormalizedObj.set("container.id", standardlibrary);
		lwcNormalizedObj.set("folder", "/Default/");
		lwcNormalizedObj.set("lifeCycleTemplate", lcTpl);
		lwcNormalizedObj.set("defaultTraceCode", TraceCode.UNTRACED);
		lwcNormalizedObj.set("endItem", false);
		lwcNormalizedObj.set("source", Source.toSource("buy"));
		lwcNormalizedObj.set("hidePartInStructure", false);
		lwcNormalizedObj.set("defaultUnit", QuantityUnit.EA);
		lwcNormalizedObj.set("FrenchDescription", sapsm_french);
		lwcNormalizedObj.set("GermanDescription", sapsm_german);
		lwcNormalizedObj.set("SpanishDescription", sapsm_spanish);
		lwcNormalizedObj.set("partType", "component");
		lwcNormalizedObj.set("StandardReferenceNumber", std.getStandardreferencenumber());
		lwcNormalizedObj.set("StatusoftheReference", std.getStatusofthereference());
		lwcNormalizedObj.set("QualifiedInsideSDRPerimeter", std.getQualifiedinsideSDRPerimeter());
		lwcNormalizedObj.set("Weight", FloatingPoint.valueOf(std.getWeight()));
		lwcNormalizedObj.set("WeightMaturity", std.getWeightmaturity());
		lwcNormalizedObj.set("CMS", std.getCms());
		lwcNormalizedObj.set("lifeCycleState", std.getLifecycle());
		lwcNormalizedObj.set("PartClassification", std.getPartClassification());
		lwcNormalizedObj.set("ReplacedStandardPart", std.isReplacedstandardpart());
		lwcNormalizedObj.set("ActivatedECDProduction", std.getActivatedECDprod());
		lwcNormalizedObj.set("ActivatedECProduction", std.getActivatedECprod());
		lwcNormalizedObj.set("WeightUnit", std.getWeightunit());
		if (std.getAdditionalattributes() != null && std.getAdditionalattributes().length > 0) {
			lwcNormalizedObj.set("AdditionalAttributes", std.getAdditionalattributes());
		}
		if (std.getStandardprogramvalidity() != null && std.getStandardprogramvalidity().length > 0) {
			lwcNormalizedObj.set("StandardProgramValidity", std.getStandardprogramvalidity());
		}
		lwcNormalizedObj.set("view", "Design");
		TypeInstanceIdentifier tii = lwcNormalizedObj.persist();
		WTPart part = (WTPart) new ReferenceFactory().getReference(tii.getPersistenceIdentifier()).getObject();
		
		WTPartMaster master = (WTPartMaster)part.getMaster();
		LWCNormalizedObject lwcObject = new LWCNormalizedObject(master,null,null,new UpdateOperationIdentifier());
		lwcObject.load("HazardousMaterialIndicator");
		lwcObject.set("HazardousMaterialIndicator", std.getHazMatInd());
		lwcObject.persist();	 
	}

	/**
	 * update an existe SAP material
	 * @param wtpartmaster the object to update
	 * @param std new data of the object
	 * @param typeprefix prefix used for test purposes. Should be an empty (but not null) String in production use;
	 * @throws Exception any exception caught is rethrown without any treatment
	 */
	private static void updateSapMaterial(WTPart wtPart, StandardDef std) throws Exception {
		
		
		LWCNormalizedObject lwcNormalizedObjRead = new LWCNormalizedObject(wtPart, null, null, null);
		
		lwcNormalizedObjRead.load(
				"name", "number", "FrenchDescription", 
				"GermanDescription", "SpanishDescription", "usedQuantityUnit", 
				"StandardReferenceNumber","StatusoftheReference", "QualifiedInsideSDRPerimeter", 
				"Weight", "WeightMaturity", "WeightUnit", 
				"CMS", "lifeCycleState", "ReplacedStandardPart",
				"ActivatedECDProduction", "ActivatedECProduction", "AdditionalAttributes", 
				"StandardProgramValidity");
		
		LWCNormalizedObject masterLwcRead = new LWCNormalizedObject(wtPart.getMaster(), null, null, null);
		masterLwcRead.load("HazardousMaterialIndicator");
		
		// PREPARE ENGLISH DESCRIPTION FOR SAP MATNR
		String sapsm_engl = std.getMpn() + "\\" + std.getEnglishdescription();
		if (sapsm_engl.length() > 40)
			sapsm_engl = StringUtils.optimizedSubstring(sapsm_engl,0, 40);

		String sapsm_french = std.getMpn() + "\\" + std.getFrenchdescription();
		if (sapsm_french.length() > 40)
			sapsm_french = StringUtils.optimizedSubstring(sapsm_french,0, 40);

		String sapsm_german = std.getMpn() + "\\" + std.getGermandescription();
		if (sapsm_german.length() > 40)
			sapsm_german = StringUtils.optimizedSubstring(sapsm_german,0, 40);

		String sapsm_spanish = std.getMpn() + "\\" + std.getSpanishdescription();
		if (sapsm_spanish.length() > 40)
			sapsm_spanish = StringUtils.optimizedSubstring(sapsm_spanish,0, 40);
		
		
		boolean updateRequired = isSapMaterialUpdateRequired(std, lwcNormalizedObjRead, masterLwcRead,sapsm_french, sapsm_german, sapsm_spanish);
		
		if( ! updateRequired) {
			System.err.println("0-updateSapMaterial not required wtpart=" + wtPart.getIdentity() + " matnr=" + std.getMatnr());
			return;
		}
			
		System.err.println("0-updateSapMaterial wtpart=" + wtPart.getIdentity() + " matnr=" + std.getMatnr());
		
		boolean bool = SessionServerHelper.manager.isAccessEnforced();
		SessionServerHelper.manager.setAccessEnforced(false);
		/*Folder folder = WorkInProgressHelper.service.getCheckoutFolder();
		CheckoutLink checkoutLink = WorkInProgressHelper.service.checkout((Workable) wtPart, folder, null);
		wtPart = (WTPart) checkoutLink.getWorkingCopy();*/
		
		//Get working copy for update
		wtPart = (WTPart)getCheckedOut(wtPart, "");
		//WorkInProgressHelper.service.undoCheckout(wtPart);
				
		LWCNormalizedObject lwcNormalizedObj = new LWCNormalizedObject(wtPart, null, null, new UpdateOperationIdentifier());
		
		lwcNormalizedObj.load(
		"name", "number", "FrenchDescription", 
		"GermanDescription", "SpanishDescription", "usedQuantityUnit", 
		"StandardReferenceNumber","StatusoftheReference", "QualifiedInsideSDRPerimeter", 
		"Weight", "WeightMaturity", "WeightUnit", 
		"CMS", "lifeCycleState", "ReplacedStandardPart",
		"ActivatedECDProduction", "ActivatedECProduction", "AdditionalAttributes", 
		"StandardProgramValidity");
		
		lwcNormalizedObj.set("FrenchDescription", sapsm_french);
		lwcNormalizedObj.set("GermanDescription", sapsm_german);
		lwcNormalizedObj.set("SpanishDescription", sapsm_spanish);
		lwcNormalizedObj.set("StandardReferenceNumber", std.getStandardreferencenumber());
		lwcNormalizedObj.set("StatusoftheReference", std.getStatusofthereference());
		lwcNormalizedObj.set("QualifiedInsideSDRPerimeter", std.getQualifiedinsideSDRPerimeter());
		lwcNormalizedObj.set("Weight", FloatingPoint.valueOf(std.getWeight()));
		lwcNormalizedObj.set("WeightMaturity", std.getWeightmaturity());
		lwcNormalizedObj.set("CMS", std.getCms());
		lwcNormalizedObj.set("lifeCycleState", std.getLifecycle());
		lwcNormalizedObj.set("ReplacedStandardPart", std.isReplacedstandardpart());
		lwcNormalizedObj.set("ActivatedECDProduction", std.getActivatedECDprod());
		lwcNormalizedObj.set("ActivatedECProduction", std.getActivatedECprod());
		lwcNormalizedObj.set("WeightUnit", std.getWeightunit());
		if (std.getAdditionalattributes() != null && std.getAdditionalattributes().length > 0) {
			lwcNormalizedObj.set("AdditionalAttributes", std.getAdditionalattributes());
		}
		if (std.getStandardprogramvalidity() != null && std.getStandardprogramvalidity().length > 0) {
			lwcNormalizedObj.set("StandardProgramValidity", std.getStandardprogramvalidity());
		}
		/*idCard = (WTDocument) idCardNormalizedObject.apply();
		try {
			idCard = (WTDocument) PersistenceHelper.manager.modify(idCard);
		*/
		Persistable sapMaterial = lwcNormalizedObj.apply();
		sapMaterial = PersistenceHelper.manager.modify(sapMaterial);
		WorkInProgressHelper.service.checkin((Workable) sapMaterial, null);
		
		WTPart part = (WTPart)sapMaterial;
		
		WTPartMaster master = (WTPartMaster)part.getMaster();
		LWCNormalizedObject lwcObject = new LWCNormalizedObject(master,null,null,new UpdateOperationIdentifier());
		lwcObject.load("HazardousMaterialIndicator");
		lwcObject.set("HazardousMaterialIndicator", std.getHazMatInd());
		lwcObject.persist();
		
		ViewHelper.service.reassignView(wtPart.getMaster(), designViewReference);
		SessionServerHelper.manager.setAccessEnforced(bool);
	}

	private static boolean isSapMaterialUpdateRequired(StandardDef std,
			LWCNormalizedObject lwcNormalizedObj, 
			LWCNormalizedObject masterLwcRead, String sapsm_french,
			String sapsm_german, String sapsm_spanish) throws WTException {
		boolean updateRequired;
		boolean updateRequiredForSinglesValues = true;
		boolean updateRequiredForMultiValuedValues = true;
		
		//System.out.println("isSapMaterialUpdateRequired");
		
		CollectionUtil collectionUtil = new CollectionUtil();
		
		String lwcAdditionalAttributes = collectionUtil.getMultiValuedAsString(lwcNormalizedObj.get("AdditionalAttributes"));
		String lwcStandardProgramValidity = collectionUtil.getMultiValuedAsString(lwcNormalizedObj.get("StandardProgramValidity"));
		
		System.out.println("- SapMaterial in database:"+
		nullToEmptyString((String)lwcNormalizedObj.get("FrenchDescription")) + "|" +
		nullToEmptyString((String)lwcNormalizedObj.get("GermanDescription")) + "|" +
		nullToEmptyString((String)lwcNormalizedObj.get("SpanishDescription")) + "|" +
		nullToEmptyString((String)lwcNormalizedObj.get("StandardReferenceNumber")) + "|" +
		nullToEmptyString((String)lwcNormalizedObj.get("StatusoftheReference")) + "|" +
		nullToEmptyString((String)lwcNormalizedObj.get("QualifiedInsideSDRPerimeter")) + "|" +
		lwcNormalizedObj.get("Weight") + "|" +
		nullToEmptyString((String)lwcNormalizedObj.get("WeightMaturity")) + "|" +
		nullToEmptyString((String)lwcNormalizedObj.get("CMS")) + "|" +
		nullToEmptyString((String)lwcNormalizedObj.get("lifeCycleState")) + "|" +
		(Boolean)lwcNormalizedObj.get("ReplacedStandardPart") + "|" +
		nullToEmptyString((String)lwcNormalizedObj.get("ActivatedECDProduction")) + "|" +
		nullToEmptyString((String)lwcNormalizedObj.get("ActivatedECProduction")) + "|" +
		nullToEmptyString((String)lwcNormalizedObj.get("WeightUnit")) + "|" +
		lwcAdditionalAttributes + "|" +
		lwcStandardProgramValidity +"|"+
		nullToEmptyString((String)masterLwcRead.get("HazardousMaterialIndicator")));
		
		String stdAdditionalAttributes = collectionUtil.getMultiValuedAsString(std.getAdditionalattributes());
		String stdStandardProgramValidity = collectionUtil.getMultiValuedAsString(std.getStandardprogramvalidity());
		
		
		System.out.println("- SapMaterial in csv file:"+
		nullToEmptyString(sapsm_french) + "|" +
		nullToEmptyString(sapsm_german) + "|" +
		nullToEmptyString(sapsm_spanish) + "|" +
		nullToEmptyString(std.getStandardreferencenumber()) + "|" +
		nullToEmptyString(std.getStatusofthereference()) + "|" +
		nullToEmptyString(std.getQualifiedinsideSDRPerimeter()) + "|" +
		std.getWeight() + "|" +
		nullToEmptyString(std.getWeightmaturity()) + "|" +
		nullToEmptyString(std.getCms()) + "|" +
		nullToEmptyString(std.getLifecycle()) + "|" +
		std.isReplacedstandardpart() + "|" +
		nullToEmptyString(std.getActivatedECDprod()) + "|" +
		nullToEmptyString(std.getActivatedECprod()) + "|" +
		nullToEmptyString(std.getWeightunit()) + "|" +
		stdAdditionalAttributes + "|" +
		stdStandardProgramValidity + "|" +
		nullToEmptyString(std.getHazMatInd()));
		//System.out.println("isSapMaterialUpdateRequired");
		
		//System.out.println("SAPNumber = " + std.getMatnr());
		
		if(areEquals((String)lwcNormalizedObj.get("FrenchDescription"),sapsm_french)
		&& areEquals((String)lwcNormalizedObj.get("GermanDescription"),sapsm_german)
		&& areEquals((String)lwcNormalizedObj.get("SpanishDescription"),sapsm_spanish)
		&& areEquals((String)lwcNormalizedObj.get("StandardReferenceNumber"),std.getStandardreferencenumber())
		&& areEquals((String)lwcNormalizedObj.get("StatusoftheReference"),std.getStatusofthereference())
		&& areEquals((String)lwcNormalizedObj.get("QualifiedInsideSDRPerimeter"),std.getQualifiedinsideSDRPerimeter())
		&& areEquals((FloatingPoint)lwcNormalizedObj.get("Weight"),FloatingPoint.valueOf(std.getWeight()))
		&& areEquals((String)lwcNormalizedObj.get("WeightMaturity"),std.getWeightmaturity())
		&& areEquals((String)lwcNormalizedObj.get("CMS"),std.getCms())
		&& areEquals((String)lwcNormalizedObj.get("lifeCycleState"),std.getLifecycle())
		&& areEquals((Boolean)lwcNormalizedObj.get("ReplacedStandardPart"), new Boolean(std.isReplacedstandardpart()))
		&& areEquals((String)lwcNormalizedObj.get("ActivatedECDProduction"),std.getActivatedECDprod())
		&& areEquals((String)lwcNormalizedObj.get("ActivatedECProduction"),std.getActivatedECprod())
		&& areEquals((String)lwcNormalizedObj.get("WeightUnit"),std.getWeightunit())
		&& areEquals((String)masterLwcRead.get("HazardousMaterialIndicator"), std.getHazMatInd())) {
			
			updateRequiredForSinglesValues = false;
		}
		
		//System.out.println("updateRequiredForSinglesValues = " + updateRequiredForSinglesValues);
		
		if (std.getAdditionalattributes() != null && std.getAdditionalattributes().length > 0) {
			
			//System.out.println("AdditionalAttributes");
			
			if(collectionUtil.equals(collectionUtil.getMultiValuedAsTable(lwcNormalizedObj.get("AdditionalAttributes"))
					,std.getAdditionalattributes())) {
				
				updateRequiredForMultiValuedValues = false;
				//System.out.println("Equals");
			}
		}
		else {
			updateRequiredForMultiValuedValues = false;
			//System.out.println("AdditionalAttributes Empty");
		}
		
		//System.out.println("updateRequiredForMultiValuedValues = " + updateRequiredForMultiValuedValues);
		
		if( ! updateRequiredForMultiValuedValues) {
			
			updateRequiredForMultiValuedValues = true;
			
			if (std.getStandardprogramvalidity() != null && std.getStandardprogramvalidity().length > 0) {
				
				//System.out.println("StandardProgramValidity");
				
				if(collectionUtil.equals(collectionUtil.getMultiValuedAsTable(lwcNormalizedObj.get("StandardProgramValidity"))
						,std.getStandardprogramvalidity())) {
					
					updateRequiredForMultiValuedValues = false;
					//System.out.println("Equals");
				}
			}
			else {
				updateRequiredForMultiValuedValues = false;
				//System.out.println("StandardProgramValidity Empty");
			}
		}
		
		//System.out.println("updateRequiredForMultiValuedValues = " + updateRequiredForMultiValuedValues);
		
		updateRequired = updateRequiredForSinglesValues || updateRequiredForMultiValuedValues;
		
		//System.out.println("updateRequired = " + updateRequired);
		
		return updateRequired;
	}

	/**
	 * Updated the name and number of a SAP Standard MAterial
	 * @param wtpartmaster the part to upadte
	 * @param std standard data to be used for udpate
	 * @param typeprefix 
	 * @param typeprefix prefix used for test purposes. Should be an empty (but not null) String in production use;
	 * @throws Exception any exception caught is rethrown without any treatment

	 */
	private static void renameSapMaterial(WTPartMaster master, StandardDef std) throws Exception {
		
		// PREPARE ENGLISH DESCRIPTION FOR SAP MATNR
		String sapsm_engl = std.getMpn() + "\\" + std.getEnglishdescription();
		if (sapsm_engl.length() > 40)
			sapsm_engl = StringUtils.optimizedSubstring(sapsm_engl,0, 40);

		String actualName = master.getName();
		if(areEquals(actualName, sapsm_engl)) {
			System.err.println("0-renameSapMaterial not required wtpart=" + master.getIdentity() + " matnr=" + std.getMatnr());
			return;
		}
		
		System.err.println("0-renameSapMaterial wtpart=" + master.getIdentity() + " matnr=" + std.getMatnr());
		
		WTPartMaster newmaster = WTPartHelper.service.changeWTPartMasterIdentity(master, sapsm_engl, null, null);
		if (newmaster != null)
			PersistenceHelper.manager.save(master);
	}

	/**
	 * creates an alternate link
	 * @param data for the alternate link to create
	 * @throws Exception
	 */
	public static void createAlternateLink(AlternateDef thisalternate) throws Exception {
		System.err.println("3-createAlternateLink origin=" + thisalternate.getOriginpn() + " target=" + thisalternate.getTargetpn());
		// get Origin Part Master
		QuerySpec qs = new QuerySpec(WTPartMaster.class);
		qs.appendWhere(new SearchCondition(WTPartMaster.class, WTPartMaster.NUMBER, SearchCondition.EQUAL, thisalternate.getOriginpn()), new int[] { 0 });
		QueryResult qr = PersistenceHelper.manager.find((StatementSpec) qs);
		WTPartMaster originpartmaster = (WTPartMaster) qr.nextElement();
		// get Target Part Master
		qs = new QuerySpec(WTPartMaster.class);
		qs.appendWhere(new SearchCondition(WTPartMaster.class, WTPartMaster.NUMBER, SearchCondition.EQUAL, thisalternate.getTargetpn()), new int[] { 0 });
		qr = PersistenceHelper.manager.find((StatementSpec) qs);
		WTPartMaster targetpartmaster = (WTPartMaster) qr.nextElement();
		// CreateAlternateLink
		WTPartAlternateLink wtpartalternatelink = WTPartAlternateLink.newWTPartAlternateLink(originpartmaster, targetpartmaster);
		wtpartalternatelink = (WTPartAlternateLink) PersistenceServerHelper.manager.store(wtpartalternatelink, null, null);
	}

	/**
	 * Creates a batch of alternate links
	 * @param alternatelist
	 * @return
	 */
	public static ArrayList<Msg> createAlternateBatch(ArrayList<AlternateDef> alternatelist) {
		ArrayList<Msg> returnmessages = new ArrayList<Msg>();
		System.err.println("[ECLOADER START CREATE ALTERNATE PART BATCH]");
		int size = alternatelist.size();
		for (int i = 0; i < size; i++) {
			AlternateDef alternateref = alternatelist.get(i);
			/* JLA
			 * 1: are replacement fields defined ?
			 * 2: does alternatelink exists ? 
			 * Yes1 + No2: create alternatelink
			 *       Yes2: and refer to the same sapmaterial ? no: ERROR4:duplicate alternate, yes: continue
			 * No1 + Yes2: delete alternatelink
			 *        No2: continue
			 */
			try {
				//JLA : Check replacement
				boolean hasreplacement = ("".equals(alternateref.getTargetpn()) ? false : true);
				if (hasreplacement) {
					//JLA : Check if alternatelink already exists
					// get targetpn alternatelink reference
					WTPartAlternateLink wtpartalternatelink = StandardLoaderTool.getAlternateLink(new AlternateDef("", alternateref.getTargetpn()));
					if (wtpartalternatelink == null)
						createAlternateLink(alternateref);
					else {
						// does refer to same alternateref ?
						wtpartalternatelink = StandardLoaderTool.getAlternateLink(alternateref);
						//Error4: another alternatelink found 
						if (wtpartalternatelink == null) {
							Msg msg = new Msg(Msg.RESULT_PROCESS_ERROR, Msg.OPERATION_ALTERNATE, "(" + alternateref.getOriginpn() + "," + alternateref.getTargetpn() + ")", String
									.format(ERROR4, alternateref.getOriginpn()), null);
							System.err.println(msg.toString());
							returnmessages.add(msg);
							throw new Exception(String.format(ERROR4, alternateref.getOriginpn()));
						}
					}
				} else {
					// get originpn alternatelink reference
					WTPartAlternateLink wtpartalternatelink = StandardLoaderTool.getAlternateLink(new AlternateDef(alternateref.getOriginpn(), ""));
					if (wtpartalternatelink != null) {
						// DeleteAlternateLink
						System.err.println("3-deleteAlternateLink origin=" + alternateref.getOriginpn());
						PersistenceServerHelper.manager.remove(wtpartalternatelink);
					}
				}
			} catch (Throwable t) {
				Msg msg = new Msg(
						Msg.RESULT_WARNING,
						Msg.OPERATION_ALTERNATE,
						"(" + alternateref.getOriginpn() + "," + alternateref.getTargetpn() + ")",
						"could not create alternate link, please check if it is present in the db. Note: the most frequent case for this error is an alternate link pointing to a part not defined as standard in SAP",
						t);
				System.err.println(msg.toString());
				t.printStackTrace();
				returnmessages.add(msg);
			}
			Msg msg = new Msg(Msg.RESULT_SUCCESS, Msg.OPERATION_ALTERNATE, "(" + alternateref.getOriginpn() + "," + alternateref.getTargetpn() + ")", null, null);
			returnmessages.add(msg);
		}
		System.err.println("[ECLOADER END CREATE ALTERNATE PART BATCH]");
		return returnmessages;
	}

	/**
	 * Creates a batch of standard parts
	 * @param standardlist the list of standard parts to be created
	 * @param typeprefix a prefix used on all part numbers to allow several tests with the same file
	 * @return
	 */
	public static ArrayList<Msg> createStandardBatch(ArrayList<StandardDef> standardlist, String typeprefix) {
		boolean enforce = SessionServerHelper.manager.setAccessEnforced(false);
		ArrayList<Msg> returnmessages = new ArrayList<Msg>();
		// -------------------------------------------
		// PREPARE LIFECYCLE TEMPLATE AND CO
		// -------------------------------------------
		LifeCycleTemplate lcTpl = null;
		System.err.println("[ECLOADER START CREATE STANDARD PART BATCH]");
		try {
			lcTpl = LifeCycleHelper.service.getLifeCycleTemplate(EC_STANDARD_PART_LC, standardlibraryref);
		} catch (Throwable t) {
			Msg msg = new Msg(Msg.RESULT_ERROR, Msg.OPERATION_PARTINIT, "N/A", "could not initialize environment for standard batch creation", t);
			t.printStackTrace();
			returnmessages.add(msg);
			return returnmessages; // if not initialized, no need to continue
		}

		// ----------------------------------------------
		// START LOOP ON MATERIAL
		// ----------------------------------------------
		if (standardlist != null)
		{
			AXLContext axlcontext = null;
			int size = standardlist.size();
			for (int i = 0; i < size; i++) {
				StandardDef standardref = standardlist.get(i);
				boolean sapstandardmaterialUpdated = false;
				boolean endCurrentRef = false;
				WTPart sapstandardmaterial = null;
				ManufacturerPartMaster mpn = null;
				
				// 0 CREATE SAP STANDARD MATERIAL
				/* JLA
				 * does sapmaterial exists ? 
				 * No:  create sapmaterial
				 * Yes: update sapmaterial
				 */
				try {
					//JLA : Check if sapstandardmaterial already exists
					// get part reference
					sapstandardmaterial = StandardLoaderTool.getPartOnNumber(standardref.getMatnr());
					if (sapstandardmaterial == null)
						//create sapstandardmaterial
						createSapMaterial(standardref, lcTpl, typeprefix);
					else {
						//Error1: sapstandardmaterial checked out 
						if (WorkInProgressHelper.isCheckedOut(sapstandardmaterial)) {
							Msg msg = new Msg(Msg.RESULT_PROCESS_ERROR, Msg.OPERATION_SAPMAT, standardref.getMatnr(), String.format(ERROR1, standardref.getMatnr()), null);
							System.err.println(msg.toString());
							returnmessages.add(msg);
							throw new Exception(String.format(ERROR1, standardref.getMatnr()));
						}
						//update sapstandardmaterial
						updateSapMaterial(sapstandardmaterial, standardref);
						renameSapMaterial(StandardLoaderTool.getPartMasterOnNumber(standardref.getMatnr()), standardref);
						sapstandardmaterialUpdated = true;
					}

				} catch (Throwable t) {
					Msg msg = new Msg(Msg.RESULT_WARNING, Msg.OPERATION_SAPMAT, standardref.getMatnr(),
							"could not create SAP Standard Material, please check if it is present in the db", t);
					System.err.println(msg.toString());
					t.printStackTrace();
					returnmessages.add(msg);
					endCurrentRef = true;
				}
				returnmessages.add(new Msg(Msg.RESULT_SUCCESS, Msg.OPERATION_SAPMAT, standardref.getMatnr(), "", null));
				/* JLA
				 * does standardpart exists ? 
				 * No:  create standardpart
				 * Yes: does same sapmaterial mpnlink 'preferred' exists ?
				 *      yes: ERROR3:duplicate mpnlink
				 *      no:  update standardpart
				 */
				try {
					if (!endCurrentRef) {
						// 1 LOOK-UP ORGANIZATION
						WTOrganization mpnorganization = StandardLoaderTool.getsupplierOrganization(standardref.getNcage());
						// 2 CREATE STANDARD PART
						//JLA : Check if StandardPart already exists
						// get manufpart reference
						ManufacturerPart standardpart = StandardLoaderTool.getManufPartOnNumber(standardref.getNcage(), standardref.getMpn());
						System.out.println("standardpart "+standardpart);
						if (standardpart == null)
							//create standardpart
							createStandardPart(standardref, lcTpl, mpnorganization, typeprefix, returnmessages);
						else {
							System.out.println("standardpart "+standardpart.getNumber());
							//Error2: standardpart checked out 
							if (WorkInProgressHelper.isCheckedOut(standardpart)) {
								Msg msg = new Msg(Msg.RESULT_PROCESS_ERROR, Msg.OPERATION_STDPART, standardref.getMatnr(), String.format(ERROR2, standardref.getMpn()), null);
								System.err.println(msg.toString());
								returnmessages.add(msg);
								endCurrentRef = true;
								throw new Exception(String.format(ERROR2, standardref.getMpn()));
							}
							// get part reference
							sapstandardmaterial = StandardLoaderTool.getPartOnNumber(standardref.getMatnr());
							if (sapstandardmaterial == null)
								throw new Exception("could not lookup WTPart for matrn = " + standardref.getMatnr());
							// get manufpart reference
							mpn = StandardLoaderTool.getManufPartOnMFRPN(standardref.getNcage(), standardref.getMpn());
							if (mpn == null)
								throw new Exception("could not lookup ManufacturerPartMaster for MFR/PN = (" + standardref.getMpn() + "," + standardref.getNcage() + ")");
							//get manufpart manufbylink
							axlcontext = AXLHelper.service.getDefaultContext(standardlibraryref);
							WTPart wtpart = StandardLoaderTool.getOEMPartManufByLink(axlcontext, mpn, AXLPreference.PREFERRED);
							boolean samesapstandardmaterial = (wtpart != null && sapstandardmaterial.getNumber().equals(wtpart.getNumber()) ? true : false);
							//Error3: preferred manufbylink to same sapmaterial already exists
							if (wtpart != null && !(sapstandardmaterialUpdated && samesapstandardmaterial)) {
								Msg msg = new Msg(Msg.RESULT_PROCESS_ERROR, Msg.OPERATION_STDPART, standardref.getMatnr(), String.format(ERROR3, standardref.getMpn()), null);
								System.err.println(msg.toString());
								returnmessages.add(msg);
								throw new Exception(String.format(ERROR3, standardref.getMpn()));
							}
							updateStandardPart(standardpart, standardref);
							if (samesapstandardmaterial)
								endCurrentRef = true;
						}
					}
				} catch (Throwable t) {
					Msg msg = new Msg(Msg.RESULT_WARNING, Msg.OPERATION_STDPART, standardref.getMatnr(),
							"could not create the sap standard part, please check if present in DB or not", t);
					System.err.println(msg.toString());
					t.printStackTrace();
					returnmessages.add(msg);
					endCurrentRef = true;
				}
				returnmessages.add(new Msg(Msg.RESULT_SUCCESS, Msg.OPERATION_STDPART, standardref.getMatnr(), "", null));
				// 3 CREATE MPNLINK
				/* JLA
				 * create mpnlink 'preferred'
				 * was sapmaterial updated ?
				 * Yes: update sapmaterial mpnlink to 'do not use'
				 */
				try {
					if (!endCurrentRef) {
						// get part reference
						sapstandardmaterial = StandardLoaderTool.getPartOnNumber(standardref.getMatnr());
						if (sapstandardmaterial == null)
							throw new Exception("could not lookup WTPart for matrn = " + sapstandardmaterial);
						if (sapstandardmaterialUpdated) {
							// get manufpart manufbylink reference
							axlcontext = AXLHelper.service.getDefaultContext(standardlibraryref);
							ManufacturerPartMaster mpnMatnr = StandardLoaderTool.getManufPartManufByLink(axlcontext, sapstandardmaterial, AXLPreference.PREFERRED);
							if (mpnMatnr == null)
								throw new Exception("could not lookup ManufacturerPartMaster for matnr = " + sapstandardmaterial.getNumber());
							//updatelink
							System.err.println("2-updateManufByLink DO_NOT_USE matnr=" + sapstandardmaterial.getNumber() + " mpn=" + mpnMatnr.getNumber());
							AXLHelper.service.setAMLPreference(axlcontext, sapstandardmaterial, mpnMatnr, AXLPreference.DO_NOT_USE);
							//update standardpart oldreference to 'yes'
							ManufacturerPart standardpart = StandardLoaderTool.getManufPartOnNumber(mpnMatnr.getOrganizationUniqueIdentifier(), mpnMatnr.getNumber());
							updateStandardPartOldReference(standardpart);
						}
						// get manufpart reference
						mpn = StandardLoaderTool.getManufPartOnMFRPN(standardref.getNcage(), standardref.getMpn());
						if (mpn == null)
							throw new Exception("could not lookup ManufacturerPartMaster for MFR/PN = (" + standardref.getMpn() + "," + standardref.getNcage() + ")");
						//createlink
						axlcontext = AXLHelper.service.getDefaultContext(standardlibraryref);
						System.err.println("2-createManufByLink PREFERRED matnr=" + sapstandardmaterial.getNumber() + " mpn=" + mpn.getNumber());
						AXLServerHelper.service.addAML(axlcontext, sapstandardmaterial, null, mpn, AXLPreference.PREFERRED);
					}
				} catch (Throwable t) {
					Msg msg = new Msg(Msg.RESULT_WARNING, Msg.OPERATION_MANUFBY, standardref.getMatnr(),
							"could not create the manufacturer link, please check if present in DB or not", t);
					System.err.println(msg.toString());
					t.printStackTrace();
					returnmessages.add(msg);
				}
				returnmessages.add(new Msg(Msg.RESULT_SUCCESS, Msg.OPERATION_MANUFBY, standardref.getMatnr(), "", null));
			}
		}
		System.err.println("[ECLOADER END CREATE STANDARD PART BATCH]");
		SessionServerHelper.manager.setAccessEnforced(enforce);
		return returnmessages;
	}

	/**
	 * Shares given Std part to the daher project
	 * 
	 * @param wtPart - object to share to daher project
	 * @param projectName - name of the project that object will be shared to
	 * @throws WTException if any...
	 * @throws IOException if any...
	 */
	public static void shareStdPartToDaherProject(WTPart wtPart) throws WTException, IOException {
		Transaction transaction = new Transaction();
		try {
			transaction.start();
			SharedContainerMap map = DataSharingHelper.service.shareVersion(wtPart, daherProjectContainerRef, daherProjectStandardFolder);
			wtPart = (WTPart) map.getShared();
			SandboxHelper.service.addToSandboxBaseline(new WTArrayList(Arrays.asList(wtPart)), daherProjectContainerRef);
			transaction.commit();
			transaction = null;
		} finally {
			if (null != transaction) {
				transaction.rollback();
			}
		}
	}

	/**
	 * Return a Std part according to its number.
	 * 
	 * @param std handle the required Std part number 
	 * @return a WTPart
	 * @throws WTException if the part isn't found or any else...
	 */
	private static WTPart getStdPart(StandardDef std) throws WTException {
		QuerySpec querySpec = new QuerySpec();
		// FROM clause
		int partIdx = querySpec.appendClassList(WTPart.class, true);
		int typeIdx = querySpec.appendClassList(WTTypeDefinition.class, false);
		int typeMasterIdx = querySpec.appendClassList(WTTypeDefinitionMaster.class, false);
		// WHERE clause: the Std part is last iteration
		SearchCondition cond = new SearchCondition(WTPart.class, WTPart.LATEST_ITERATION, SearchCondition.IS_TRUE);
		querySpec.appendWhere(cond, new int[] { partIdx });
		// WHERE clause: join part to type def
		cond = new SearchCondition(WTPart.class, WTPart.NUMBER, SearchCondition.EQUAL, std.getMpn());
		querySpec.appendAnd();
		querySpec.appendWhere(cond, new int[] { partIdx });
		// WHERE clause: join part to type def
		cond = new SearchCondition(WTPart.class, Typed.TYPE_DEFINITION_REFERENCE + "." + WTAttributeNameIfc.REF_OBJECT_ID, WTTypeDefinition.class, WTAttributeNameIfc.ID_NAME);
		querySpec.appendAnd();
		querySpec.appendWhere(cond, new int[] { partIdx, typeIdx });
		// WHERE clause: join type to type master
		cond = new SearchCondition(WTTypeDefinition.class, WTTypeDefinition.MASTER_REFERENCE + "." + WTAttributeNameIfc.REF_OBJECT_ID, WTTypeDefinitionMaster.class,
				WTAttributeNameIfc.ID_NAME);
		querySpec.appendAnd();
		querySpec.appendWhere(cond, new int[] { typeIdx, typeMasterIdx });
		// WHERE clause: the type name
		cond = new SearchCondition(WTTypeDefinitionMaster.class, WTTypeDefinitionMaster.HIERARCHY_DISPLAY_NAME_KEY, SearchCondition.LIKE, "%StandardPart");
		querySpec.appendAnd();
		querySpec.appendWhere(cond, new int[] { typeMasterIdx });
		// WHERE clause: the type is last iteration
		cond = new SearchCondition(WTTypeDefinition.class, WTTypeDefinition.LATEST_ITERATION, SearchCondition.IS_TRUE);
		querySpec.appendAnd();
		querySpec.appendWhere(cond, new int[] { typeIdx });
		//
		QueryResult queryResult = PersistenceHelper.manager.find((StatementSpec) querySpec);
		//
		if (!queryResult.hasMoreElements()) {
			throw new WTException("Unable to find freshly created Std Part: " + std.getMpn());
		}
		return (WTPart) ((Persistable[]) queryResult.nextElement())[0];
	}
	
	
	public static String nullToEmptyString(String value) {
		
		if(value == null) {
			return "";
		}
		else {
			return value;
		}
	}
	
	public static boolean areEquals(String value1, String value2) {
		return nullToEmptyString(value1).equals(nullToEmptyString(value2));
	}
	
	public static boolean areEquals(FloatingPoint value1, FloatingPoint value2) {
		return (value1 == null && value2 == null) || (value1 != null && value1.equals(value2));
	}
	
	public static boolean areEquals(Boolean value1, Boolean value2) {
		return (value1 == null && value2 == null) || (value1 != null && value2 != null && value1.booleanValue() == value2.booleanValue());
	}
	
	/**
	 * Check out the workable if needed and return the working copy.
	 * 
	 * @param p_workable
	 * @param message
	 * @return
	 * @throws WTException
	 * @throws WTPropertyVetoException
	 */
	public static Workable getCheckedOut(Workable p_workable, String message) throws WTException, WTPropertyVetoException {

		Workable workable;

		if (!WorkInProgressHelper.isCheckedOut(p_workable)) {
			try {
				CheckoutLink cl = WorkInProgressHelper.service.checkout(p_workable, WorkInProgressHelper.service.getCheckoutFolder(), message);
				workable = cl.getWorkingCopy();
			} catch (ObjectsAlreadyCheckedOutException e) {
				workable = WorkInProgressHelper.service.workingCopyOf(p_workable);
			}
		} else {
			try {
				workable = WorkInProgressHelper.service.workingCopyOf(p_workable);
			} catch (WorkInProgressException e) {
				workable = p_workable;
			}
		}

		return workable;
	}
	/*System.out.println("##########################################################");
	System.out.println(std.getActivatedECDprod());
	System.out.println("##########################################################");*/
	/*System.out.println("SAPNumber = " + std.getMatnr());
	
	updateRequiredForSinglesValues = updateRequiredForSinglesValues || !lwcNormalizedObj.get("FrenchDescription").equals(sapsm_french);
	System.out.println("updateRequiredForSinglesValues = " + updateRequiredForSinglesValues);
	System.out.println("lwcNormalizedObj.get(FrenchDescription) = " + lwcNormalizedObj.get("FrenchDescription"));
	System.out.println("sapsm_french = " + sapsm_french);
	updateRequiredForSinglesValues = updateRequiredForSinglesValues || !lwcNormalizedObj.get("GermanDescription").equals(sapsm_german);
	System.out.println("updateRequiredForSinglesValues = " + updateRequiredForSinglesValues);
	updateRequiredForSinglesValues = updateRequiredForSinglesValues || !lwcNormalizedObj.get("SpanishDescription").equals(sapsm_spanish);
	System.out.println("updateRequiredForSinglesValues = " + updateRequiredForSinglesValues);
	updateRequiredForSinglesValues = updateRequiredForSinglesValues || !lwcNormalizedObj.get("StandardReferenceNumber").equals(std.getStandardreferencenumber());
	System.out.println("updateRequiredForSinglesValues = " + updateRequiredForSinglesValues);
	updateRequiredForSinglesValues = updateRequiredForSinglesValues || !lwcNormalizedObj.get("StatusoftheReference").equals(std.getStatusofthereference());
	System.out.println("updateRequiredForSinglesValues = " + updateRequiredForSinglesValues);
	updateRequiredForSinglesValues = updateRequiredForSinglesValues || !lwcNormalizedObj.get("QualifiedInsideSDRPerimeter").equals(std.getQualifiedinsideSDRPerimeter());
	System.out.println("updateRequiredForSinglesValues = " + updateRequiredForSinglesValues);
	updateRequiredForSinglesValues = updateRequiredForSinglesValues || !lwcNormalizedObj.get("Weight").equals(FloatingPoint.valueOf(std.getWeight()));
	System.out.println("updateRequiredForSinglesValues = " + updateRequiredForSinglesValues);
	updateRequiredForSinglesValues = updateRequiredForSinglesValues || !lwcNormalizedObj.get("WeightMaturity").equals(std.getWeightmaturity());
	System.out.println("updateRequiredForSinglesValues = " + updateRequiredForSinglesValues);
	updateRequiredForSinglesValues = updateRequiredForSinglesValues || !lwcNormalizedObj.get("CMS").equals(std.getCms());
	System.out.println("updateRequiredForSinglesValues = " + updateRequiredForSinglesValues);
	updateRequiredForSinglesValues = updateRequiredForSinglesValues || !lwcNormalizedObj.get("lifeCycleState").equals(std.getLifecycle());
	System.out.println("updateRequiredForSinglesValues = " + updateRequiredForSinglesValues);
	updateRequiredForSinglesValues = updateRequiredForSinglesValues || !(Boolean)lwcNormalizedObj.get("ReplacedStandardPart") == std.isReplacedstandardpart();
	System.out.println("updateRequiredForSinglesValues = " + updateRequiredForSinglesValues);
	updateRequiredForSinglesValues = updateRequiredForSinglesValues || !lwcNormalizedObj.get("ActivatedECDProduction").equals(std.getActivatedECDprod());
	System.out.println("updateRequiredForSinglesValues = " + updateRequiredForSinglesValues);
	updateRequiredForSinglesValues = updateRequiredForSinglesValues || !lwcNormalizedObj.get("ActivatedECProduction").equals(std.getActivatedECprod());
	System.out.println("updateRequiredForSinglesValues = " + updateRequiredForSinglesValues);
	updateRequiredForSinglesValues = updateRequiredForSinglesValues || !lwcNormalizedObj.get("WeightUnit").equals(std.getWeightunit());
	System.out.println("updateRequiredForSinglesValues = " + updateRequiredForSinglesValues);
	
	CollectionUtil collectionUtil = new CollectionUtil();*/
}
