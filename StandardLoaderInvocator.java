
package ext.eurocopter.x4.loaders.ecloader;

import ext.eurocopter.helper.AuthenticationHelper;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.apache.log4j.Logger;
import wt.method.RemoteAccess;
import wt.util.WTException;


public class StandardLoaderInvocator
    implements RemoteAccess  {

	 private static Logger logger = Logger.getLogger(ext.eurocopter.x4.loaders.ecloader.StandardLoaderInvocator.class.getName());
	 
	public StandardLoaderInvocator()
    {
    }

    public static Object execute(Boolean isDevEnvironment, String methodName, Class methodParamDescriptor[], Object methodParam[])
        throws WTException, IOException
    {
        AuthenticationHelper.forceConnectionValidation();
        try
        {
            ClassLoader parentClassLoader = ext.eurocopter.tools.CorePDMClassLoader.class.getClassLoader();
            Class standardLoaderClass = null;
            String scriptClassName = "ext.eurocopter.x4.loaders.ecloader.StandardLoader";
            if(isDevEnvironment.booleanValue())
            {
                ECClassLoader eCClassLoader = new ECClassLoader(parentClassLoader);
                standardLoaderClass = eCClassLoader.loadClass(scriptClassName);
            } else
            {
                standardLoaderClass = parentClassLoader.loadClass(scriptClassName);
            }
            Method method;
            if(isDevEnvironment.booleanValue())
            {
                method = standardLoaderClass.getMethod("InitializeData", new Class[0]);
                method.invoke(null, new Object[0]);
            }
            method = standardLoaderClass.getMethod(methodName, methodParamDescriptor);
            Object o = method.invoke(null, methodParam);
            return o;
        }
        catch(ClassNotFoundException e)
        {
            logger.error(e);
        }
        catch(IllegalAccessException e)
        {
            logger.error(e);
        }
        catch(IllegalArgumentException e)
        {
            e.printStackTrace();
        }
        catch(InvocationTargetException e)
        {
            e.printStackTrace();
        }
        catch(NoSuchMethodException e)
        {
            e.printStackTrace();
        }
        return null;
    }

}
