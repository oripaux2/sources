package ext.eurocopter.x4.loaders.ecloader;


import wt.fc.Persistable;
import wt.fc.PersistenceHelper;
import wt.fc.QueryResult;
import wt.inf.container.WTContainer;
import wt.org.WTOrganization;
import wt.org.WTOrganizationIdentifier;
import wt.part.WTPart;
import wt.part.WTPartAlternateLink;
import wt.part.WTPartHelper;
import wt.part.WTPartMaster;
import wt.pds.StatementSpec;
import wt.query.QuerySpec;
import wt.query.SearchCondition;
import wt.type.TypedUtilityServiceHelper;
import wt.util.WTException;
import wt.vc.Iterated;

import com.ptc.windchill.suma.axl.AXLContext;
import com.ptc.windchill.suma.axl.AXLEntry;
import com.ptc.windchill.suma.axl.AXLPreference;
import com.ptc.windchill.suma.axl.AXLServerHelper;
import com.ptc.windchill.suma.part.ManufacturerPart;
import com.ptc.windchill.suma.part.ManufacturerPartMaster;
import com.ptc.windchill.suma.supplier.Manufacturer;
import com.ptc.windchill.suma.supplier.SupplierHelper;

/**
 * This class gathers static helper methods to access Windchill objects
 * @author Nicolas de Mauroy
 *
 */
public class StandardLoaderTool {

	/**
	 * Gets the manufacturer part master from its NCAGE and number
	 * @param ncage the ncage of the manufacturer part organization
	 * @param number the number (local ot the organization) of 
	 * @return the latest iteration of the manufacturer part, null if not found
	 * @throws Exception all iterations are rethrown
	 */
	public static ManufacturerPartMaster getManufPartOnMFRPN(String ncage,String number) throws Exception {
		QuerySpec queryPart = new QuerySpec(ManufacturerPartMaster.class);
		queryPart.appendWhere(new SearchCondition(ManufacturerPartMaster.class, ManufacturerPartMaster.NUMBER, SearchCondition.EQUAL,number), new int[]{0});
		QueryResult qrmpnresult = PersistenceHelper.manager.find((StatementSpec) queryPart);
		while (qrmpnresult.hasMoreElements()) {
			ManufacturerPartMaster thiselement=(ManufacturerPartMaster) qrmpnresult.nextElement();
			//System.err.println("getManufPartOnMFRPN wtpart="+number+" ncage="+ncage+" found.");	
			if (thiselement.getOrganizationUniqueIdentifier().compareTo(ncage)==0) return thiselement;
		}
		//System.err.println("getManufPartOnMFRPN wtpart="+number+" ncage="+ncage+" not found.");	
		return null;
	}

	/**
	 * Gets the latest manufacturer part iteration from its NCAGE and number
	 * @param ncage the ncage of the manufacturer part organization
	 * @param number the number (local ot the organization) of 
	 * @return the latest iteration of the manufacturer part, null if not found
	 * @throws Exception all iterations are rethrown
	 */
	public static ManufacturerPart getManufPartOnNumber(String ncage,String number) throws Exception {
		QuerySpec queryPart = new QuerySpec(ManufacturerPart.class);
		queryPart.appendWhere(new SearchCondition(ManufacturerPart.class, ManufacturerPart.NUMBER, SearchCondition.EQUAL,number), new int[]{0});
		queryPart.appendAnd();
		queryPart.appendWhere(new SearchCondition(ManufacturerPart.class, Iterated.LATEST_ITERATION, SearchCondition.IS_TRUE), new int[]{0});
		QueryResult qrmpnresult = PersistenceHelper.manager.find((StatementSpec) queryPart);
		while (qrmpnresult.hasMoreElements()) {
			ManufacturerPart thiselement=(ManufacturerPart) qrmpnresult.nextElement();
			//System.err.println("getManufPartOnNumber wtpart="+number+" ncage="+ncage+" found.");
			if (thiselement.getOrganizationUniqueIdentifier().compareTo(ncage)==0) return thiselement;
		}
		//System.err.println("getManufPartOnNumber wtpart="+number+" ncage="+ncage+" not found.");
		return null;
	}

	/**
	 * gets the OEM part from the manufacturer part depending on the preference status
	 * @param context generic context. No useful info passed
	 * @param manufpartmaster the master of the manufacturer part
	 * @param preference the status of the SUMA link (prefered ...)
	 * @return the OEM part if found, else null
	 * @throws Exception all iterations are rethrown
	 */
	static WTPart getOEMPartManufByLink(AXLContext context, ManufacturerPartMaster manufpartmaster, AXLPreference preference) throws Exception {
		//get AML link
		QuerySpec qs = new QuerySpec();
		int idaxl = qs.appendClassList(AXLEntry.class, true);
		int idpart = qs.appendClassList(WTPart.class, false);
		qs.setAdvancedQueryEnabled(true);
		qs.appendWhere(new SearchCondition(AXLEntry.class, "contextReference.key.id", "=", context.getPersistInfo().getObjectIdentifier().getId()), new int[] { idaxl });
		qs.appendAnd();
		qs.appendWhere(new SearchCondition(AXLEntry.class, "manufacturerPartReference.key.id", "=", manufpartmaster.getPersistInfo().getObjectIdentifier().getId()), new int[] { idaxl });
		qs.appendAnd();
		qs.appendWhere(new SearchCondition(AXLEntry.class, "oemPartReference.key.id", WTPart.class, "thePersistInfo.theObjectIdentifier.id"), new int[] { idaxl, idpart });
		qs.appendAnd();
		qs.appendWhere(new SearchCondition(WTPart.class, "iterationInfo.latest", "TRUE"), new int[] { idpart });
	
		QueryResult qr = PersistenceHelper.manager.find((StatementSpec)qs);
		while (qr.hasMoreElements()) {
			AXLEntry entry = (AXLEntry)((Persistable[])qr.nextElement())[0];
			WTPart wtpart = entry.getOemPart();
			AXLPreference axlpreference = AXLServerHelper.service.getAMLPreference(context, wtpart, manufpartmaster);
			//System.err.println("getOEMPartManufByLink mpn="+ manufpartmaster.getNumber()+" wtpart="+wtpart.getNumber()+" preference="+axlpreference+" found.");	
			if (axlpreference!=null && preference.compareTo(axlpreference)==0) return wtpart;
		}
		//System.err.println("getOEMPartManufByLink mpn="+manufpartmaster.getNumber()+" preference="+preference+" not found.");
		return null;
	}

	/**
	 * Gets a part latest iteration based on number
	 * @param number a string indicating the Windchill part number
	 * @return the windchill part if found. If not found, an exception is returned;
	 * @throws Exception any expection caught is rethrown without any treatment
	 */
	public static WTPart getPartOnNumber(String number) throws Exception {
		QuerySpec queryspec = new QuerySpec(WTPart.class);
		queryspec.appendWhere(new SearchCondition(WTPart.class, WTPart.NUMBER, SearchCondition.EQUAL,number), new int[]{0});
		queryspec.appendAnd();
		queryspec.appendWhere(new SearchCondition(WTPart.class, Iterated.LATEST_ITERATION, SearchCondition.IS_TRUE), new int[]{0});
		QueryResult qrpart = PersistenceHelper.manager.find((StatementSpec) queryspec);
		while (qrpart.hasMoreElements()) {
			WTPart thispart = (WTPart) qrpart.nextElement();
			//System.err.println("getPartOnNumber wtpart="+number+" found.");
			if(TypedUtilityServiceHelper.service.getExternalTypeIdentifier(thispart).endsWith("SAPStandardMaterial")) {
				return thispart;
			}
		}
		//System.err.println("getPartOnNumber wtpart="+number+" not found.");
		return null;
	}

	/**
	 * Gets the manufacturer part from the OEM Part
	 * @param context generic context. No useful info passed
	 * @param wtpart the iteration of the OEM Part
	 * @param preference the status of the SUMA link (prefered ...)
	 * @return the manufacturer part if found, else null
	 * @throws Exception all iterations are rethrown
	 */
	static ManufacturerPartMaster getManufPartManufByLink(AXLContext context, WTPart wtpart, AXLPreference preference) throws Exception {
		//get AML link
		QuerySpec qs = new QuerySpec();
		int idaxl = qs.appendClassList(AXLEntry.class, true);
		int idpart = qs.appendClassList(WTPart.class, false);
		qs.appendWhere(new SearchCondition(AXLEntry.class, "contextReference.key.id", "=", context.getPersistInfo().getObjectIdentifier().getId()), new int[] { idaxl });
		qs.appendAnd();
		qs.appendWhere(new SearchCondition(AXLEntry.class, "oemPartReference.key.id", WTPart.class, "thePersistInfo.theObjectIdentifier.id"), new int[] { idaxl, idpart });
		qs.appendAnd();
		qs.appendWhere(new SearchCondition(WTPart.class, "master>number", "=", wtpart.getNumber()), new int[] { idpart });
	
		QueryResult qr = PersistenceHelper.manager.find((StatementSpec)qs);
		while (qr.hasMoreElements()) {
			AXLEntry entry = (AXLEntry)((Persistable[])qr.nextElement())[0];
			ManufacturerPartMaster manufpartmaster = entry.getManufacturerPart();
			AXLPreference axlpreference = AXLServerHelper.service.getAMLPreference(context, wtpart, manufpartmaster);
			//System.err.println("getManufPartManufByLink mpn="+manufpartmaster.getNumber()+" wtpart="+wtpart.getNumber()+" preference="+axlpreference+" found.");
			if (axlpreference!=null && preference.compareTo(axlpreference)==0) return manufpartmaster;
		}
		//System.err.println("getManufPartManufByLink wtpart="+wtpart.getNumber()+" preference="+preference+" not found.");
		return null;
	}

	/**
	 * Gets an alternate link from the definition (two OEM parts it links) 
	 * @param thisalternate data on the alternate link
	 * @return the alternate link, else returns null
	 * @throws Exception any exception caugt is thrown
	 */
	static WTPartAlternateLink getAlternateLink(AlternateDef thisalternate) throws Exception {
		WTPartMaster originpartmaster = null;
		WTPartMaster targetpartmaster = null;
		// get Origin Part Master
		if (!"".equals(thisalternate.getOriginpn())) {
			QuerySpec qsorg = new QuerySpec(WTPartMaster.class);
			qsorg.appendWhere(new SearchCondition(WTPartMaster.class, WTPartMaster.NUMBER, SearchCondition.EQUAL,thisalternate.getOriginpn()), new int[]{0});
			QueryResult qrorg = PersistenceHelper.manager.find((StatementSpec) qsorg);
			try{
				originpartmaster = (WTPartMaster) qrorg.nextElement();
			}
			catch(Exception e){
				throw new Exception("The origin part "+thisalternate.getOriginpn()+" doesn't exist! \n"+e);
			}
			//System.err.println("getAlternateLink master wtpartorig="+originpartmaster.getNumber());
		}
		// get Target Part Master
		if (!"".equals(thisalternate.getTargetpn())) {
			QuerySpec qsdst = new QuerySpec(WTPartMaster.class);
			qsdst.appendWhere(new SearchCondition(WTPartMaster.class, WTPartMaster.NUMBER, SearchCondition.EQUAL,thisalternate.getTargetpn()), new int[]{0});
			QueryResult qrdst = PersistenceHelper.manager.find((StatementSpec) qsdst);
			try{
				targetpartmaster = (WTPartMaster) qrdst.nextElement();
			}
			catch(Exception e){
				throw new Exception("The target part "+thisalternate.getTargetpn()+" doesn't exist! \n"+e);
			}
			//System.err.println("getAlternateLink master wtparttarget="+targetpartmaster.getNumber());
		}
		// get Origin Part Master Alternate Link
		if ("".equals(thisalternate.getOriginpn())) {
			QueryResult qr=WTPartHelper.service.getAlternateForWTPartMasters(targetpartmaster);
			if (qr.hasMoreElements()) originpartmaster=(WTPartMaster) qr.nextElement();
			//System.err.println("getAlternateLink alternatefor wtpartorig="+(originpartmaster!=null?originpartmaster.getNumber():"not found"));
		}
		// get Target Part Master Alternate Link
		else if ("".equals(thisalternate.getTargetpn())) {
			QueryResult qr=WTPartHelper.service.getAlternatesWTPartMasters(originpartmaster);
			if (qr.hasMoreElements()) targetpartmaster=(WTPartMaster) qr.nextElement();
			//System.err.println("getAlternateLink alternates wtparttarget="+(targetpartmaster!=null?targetpartmaster.getNumber():"not found"));
		}
		// findAlternateLink
		if (originpartmaster!=null && targetpartmaster!=null) {
			QueryResult qr=PersistenceHelper.manager.find(WTPartAlternateLink.class, targetpartmaster, "alternates", originpartmaster);
			if (qr.hasMoreElements()) {
				WTPartAlternateLink alternate=(WTPartAlternateLink) qr.nextElement();
				//System.err.println("getAlternateLink wtpartorig="+originpartmaster.getNumber()+" wtparttarget="+targetpartmaster.getNumber()+" found.");
				return alternate;
			}
			//System.err.println("getAlternateLink find wtpartorig="+originpartmaster.getNumber()+" wtparttarget="+targetpartmaster.getNumber()+" empty");
		}
		//System.err.println("getAlternateLink wtpartorig="+thisalternate.getOriginpn()+" wtparttarget="+thisalternate.getTargetpn()+" not found.");
		return null;
	}

	/**
	 * Gets a part master based on the number
	 * @param number a string indicating the Windchill part number
	 * @return the windchill part if found. If not found, an exception is returned;
	 * @throws Exception any expection caught is rethrown without any treatment
	 */
	static WTPartMaster getPartMasterOnNumber(String number) throws Exception {
		QuerySpec qs = new QuerySpec(WTPartMaster.class);
		qs.appendWhere(new SearchCondition(WTPartMaster.class, WTPartMaster.NUMBER, SearchCondition.EQUAL, number), null);
		QueryResult qr = PersistenceHelper.manager.find((StatementSpec)qs);
		return (WTPartMaster) qr.nextElement();
	}

	/**
	 * @param ncage  the ncage identifier of the supplier organization
	 * @return the organization
	 * @throws WTException
	 */
	public static WTOrganization getsupplierOrganization(String ncage) throws Exception {
		WTOrganizationIdentifier orgId = WTOrganizationIdentifier.newWTOrganizationIdentifier();
		orgId.setCodingSystem("0141");
		orgId.setUniqueIdentifier(ncage);
		WTOrganization wtorg = wt.ixb.publicforhandlers.IxbHndHelper.getOrganizationByGlobalOrgId(orgId);
		return wtorg;
	}
	
	/**
	 * A method to query a manufacturer based on the supplier organization
	 * @param supplierorg
	 * @return
	 * @throws Exception
	 */
	static Manufacturer getManufacturerOnOrganization(WTOrganization supplierorg,WTContainer suppliercontainer) throws Exception {
		return SupplierHelper.service.getManufacturer(supplierorg,suppliercontainer);
	
	}

}
