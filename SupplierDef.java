package ext.eurocopter.x4.loaders.ecloader;

import java.io.Serializable;

/**
 * The data definition to load a supplier in Windchill using the
 * Standard Loader.
 * 
 * @author Nicolas de Mauroy
 *
 */
public class SupplierDef implements Serializable {
	private static final long serialVersionUID = -4753312986784660654L;
	private String ncage;

	/**
	 * @return the NCAGE code of the organization
	 */
	public String getNcage() {
		return ncage;
	}

	/**
	 * @param ncage the NCAGE code of the organization
	 */
	public SupplierDef(String ncage) {
		super();
		this.ncage = ncage;
	}
	
}
