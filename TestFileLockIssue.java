package ext.eurocopter.x4.loaders.ecloader;

import java.io.File;
import java.io.FileWriter;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TestFileLockIssue {
	private static Logger LOGGER = Logger.getAnonymousLogger();
	public static void main(String [] args) {
		System.out.println("This program investigates the 'system lock table is full' issue");
		if (args.length!=1) {
			System.out.println("Usage java TestFileLockIssue testpath");
			System.exit(1);
		}
		String path = args[0];
		
		System.out.println("Attempts to create a log file in the following location : "+path);
		FileHandler logfilehandler=null;
	
			try {	
				logfilehandler=new FileHandler(path+File.separator+"ECLoaderlog%g.log",10000000,1000,true);
				logfilehandler.setLevel(Level.ALL);
				logfilehandler.setFormatter(new ECFormatter());
				//LOGGER.setUseParentHandlers(false);
				LOGGER.addHandler(logfilehandler);
				LOGGER.severe("Test of a first log");
				System.out.println("Established logging");
			} catch (Throwable T) {
				System.out.println("unexpected error during execution "+T.getMessage());
				LOGGER.severe("unexpected error during execution "+T.getMessage());
				for (int i=0;i<T.getStackTrace().length;i++) {
					LOGGER.warning("detailed stacktrace: "+T.getStackTrace()[i].toString());
				}
				if (logfilehandler!=null) logfilehandler.close();
			}
			System.out.println("second try of filelog with the same path but no append : "+path);
			try {	
				logfilehandler=new FileHandler(path+File.separator+"ECLoaderlog%g.log",10000000,1000);
				logfilehandler.setLevel(Level.ALL);
				logfilehandler.setFormatter(new ECFormatter());
				//LOGGER.setUseParentHandlers(false);
				LOGGER.addHandler(logfilehandler);
				LOGGER.severe("Test of a first log");
				System.out.println("Established logging");
			} catch (Throwable T) {
				System.out.println("unexpected error during execution "+T.getMessage());
				LOGGER.severe("unexpected error during execution "+T.getMessage());
				for (int i=0;i<T.getStackTrace().length;i++) {
					LOGGER.warning("detailed stacktrace: "+T.getStackTrace()[i].toString());
				}
				if (logfilehandler!=null) logfilehandler.close();
			}
			
			System.out.println("Attempts to create a local file with append = true at the same path : ");
			try {
				FileWriter thisfilewriter = new FileWriter(new File(path+File.separator+"filename.toto"),true);
				thisfilewriter.write("toto");
				thisfilewriter.close();
			} catch (Throwable T) {
				System.out.println("unexpected error during execution "+T.getMessage());
				LOGGER.severe("unexpected error during execution "+T.getMessage());
				for (int i=0;i<T.getStackTrace().length;i++) {
					LOGGER.warning("detailed stacktrace: "+T.getStackTrace()[i].toString());
				}
				if (logfilehandler!=null) logfilehandler.close();
			}
			
			System.out.println("Attempts to create a local file without append = true at the same path : ");
			try {
				FileWriter thisfilewriter = new FileWriter(new File(path+File.separator+"filename.toto"));
				thisfilewriter.write("toto");
				thisfilewriter.close();
			} catch (Throwable T) {
				System.out.println("unexpected error during execution "+T.getMessage());
				LOGGER.severe("unexpected error during execution "+T.getMessage());
				for (int i=0;i<T.getStackTrace().length;i++) {
					LOGGER.warning("detailed stacktrace: "+T.getStackTrace()[i].toString());
				}
				if (logfilehandler!=null) logfilehandler.close();
			}
			
			
			LOGGER.info("Standard loading operation is now terminated. Please transmit the log file to the business users for analysis.");
			logfilehandler.close();
		}
	}

